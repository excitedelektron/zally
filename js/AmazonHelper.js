﻿var AmazonHelper = function (url, $html) {

    var _root = this;

    // keepa api key 8jbces27j86mqkijcfeb1vtrgh75eh21r0f0t6dbf16mf1ok2jd7vhmnn4e9nbrt

    // getProduct
    _root.getProduct = function ($html, country, callBack) {

        chrome.runtime.sendMessage({ action: "GetBasicInfo", asin: _root.getASIN(), html: $html, country: country }, function (response) {

            if (typeof response !== 'undefined') {
                if (response.status == 'success') {
                    callBack(response.data);
                    return;
                }
            }

            callBack(false);
        });
    };

    // getProductByASIN
    _root.getProductByASIN = function (asin, country, callBack) {

        country = country || "com";

        var url = "https://www.amazon." + country + "/dp/" + asin;

        $.get(url, function (result) {

            _root.getProduct(result, country, callBack);

        });
    };

    // getASIN
    _root.getASIN = function (html) {

        return $(html).find("input#ASIN").length > 0 ? $(html).find("input#ASIN").val() : null;
    };

    // getOffers
    _root.getOffers = function (asin, country, callBack) {

        var offers = [];

        country = country || "com";

        var url = "https://www.amazon." + country + "/gp/offer-listing/" + asin + "/ref=dp_olp_new_mbc?ie=UTF8&condition=new";       

        $.get(url, function (html) {

            $(html).find(".olpOffer").each(function () {

                var soldByAmazon = !!$(this).find('.olpSellerName img[alt^="Amazon"]')[0];                
                var prime= $(this).find(".supersaver").length > 0;
                var base_price= Number($(this).find(".olpOfferPrice").text().trim().replace(/[^0-9\.]+/g, ""));
                var shipping = Number($(this).find(".olpShippingPrice").first().text().trim().replace(/[^0-9\.]+/g, ""));
                var price = (base_price + shipping).toFixed(2);

                offers.push({
                    soldByAmazon: soldByAmazon,
                    prime: prime,
                    base_price: base_price,
                    shipping: shipping,
                    price: (base_price + shipping).toFixed(2)
                });
            });

            var lnkNextPage = $(html).find("ul.a-pagination li.a-last a");

            var nextPageUrl = lnkNextPage.length > 0 ? lnkNextPage.attr("href") : false;

            callBack(offers, nextPageUrl);
        });
    };

    // extractCountryFromURL
    _root.extractCountryFromURL = function (url) {

        if (!url) return null;

        var found_country = '';
        var regex = /.+amazon\.([a-z.]{2,6})\/.*/;
        var matches = url.match(regex);

        if (matches.length == 0) {
            return null;
        }

        var found_country = matches[1];

        return found_country;
    };
};