var filename = getCurrentDate();
chrome.runtime.onMessage.addListener
(
    function (request, sender, sendResponse)
    {
        if(request.type == 'product_info')
        {
            
            
        }
        else if(request.type === 'search_done')
        {
            alert("Search completed successfully!");
        }
        else if(request.type === 'storefront')
        {
            filename = request.data + '_' + getCurrentDate();
        }
    }
);


function loadSaved()
{
    chrome.storage.local.get(
    {
        saved_items: []
    }, function(loaded){
        var items = loaded.saved_items;
        
        if(!items)
            return;
        
        for(var i=0; i < items.length; i++)
        {
            var item = items[i];
            
            if(item == null)
                continue;
            
            var html = `<div class="ap__saved-table__line" id="`+ item.id +`">
                    <div class="ap__col ap__col--1">
                        <a href="#" class="ap__col-image"><img src="`+ item.image +`" alt=""></a>
                    </div>
                    <div class="ap__col ap__col--2">
                        <h2 class="ap__col-title"><a href="`+ item.url+`" target='_blank'>`+ item.title +`</a></h2>
                    </div>
                    <div class="ap__col ap__col--3">
                        <p>`+ item.asin +`</p>
                    </div>
                    <div class="ap__col ap__col--4">
                        <p>`+ item.sales_rank+`</p>
                    </div>
                    <div class="ap__col ap__col--5">
                        <p><span class="ap__col-valute">$</span> `+ item.buy_price +`</p>
                    </div>
                    <div class="ap__col ap__col--6">
                        <p><span class="ap__col-valute">$</span> `+ item.sell_price +`</p>
                    </div>
                    <div class="ap__col ap__col--7">
                        <span class="ap__roi `+ getROIColor(item.roi) +`">`+ item.roi +`%</span>
                    </div>
                    <div class="ap__col ap__col--8">
                        <p><span class="ap__col-valute">$</span> `+ item.profit +`</p>
                    </div>
                    <div class="ap__col ap__col--9">
                        <p>`+ item.date +`</p>
                    </div>
                    <div class="ap__col ap__col--9_5 notes">
                        <p>`+ item.notes +`</p>
                    </div>
                    <div class="ap__col ap__col--10">
                        <a href="`+ item.stockstats_link +`" class="ap__statistics" target='_blank'></a>
                    </div>
                    <div class="ap__col ap__col--11">
                        <a href="#" class="ap__remove"></a>
                    </div>
                </div>`;

//            var html = "<tr>";
//
//            html += "<td><a target='_blank' href='" + seller.url + "' >" + seller.asin +"</a></td>";
//            html += "<td>"+ seller.buy_price +"</td>";
//            html += "<td>"+ seller.sell_price +"</td>";
//            html += "<td>"+ seller.sales_rank +"</td>";
//            html += "<td>"+ seller.roi +"</td>";
//            html += "<td>"+ seller.profit +"</td>";
//            html += "<td>"+ seller.date +"</td>";
//            html += "<td>"+ seller.notes +"</td>";
//
//            html += "</tr>";

            $(".ap__saved-table").append(html);
        }
    });
    
}

function getROIColor(roi)
{
    roi = parseFloat(roi);
    var roi_class = 'ap__roi-color-1';
    if(roi < 0)
        roi_class = 'ap__roi-color-1';
    else if(roi >= 0 && roi < 10)
        roi_class = 'ap__roi-color-2';
    else if(roi >= 10 && roi < 50)
        roi_class = 'ap__roi-color-3';
    else if(roi >= 50 && roi < 100)
        roi_class = 'ap__roi-color-4';
    else
        roi_class = 'ap__roi-color-5';
    
    return roi_class;
}

$(document).ready(function(){
  
    loadSaved();
    
    // Load Theme
    getTheme(function(theme){
        if(theme)
        {
            $(".ap__box").removeClass("ap__box--dark");
            $(".ap__box").removeClass("ap__box--white");
            $(".ap__box").addClass('ap__box--' + theme);
            
            if(theme == 'white')
            {
                $('.ap__dark').removeClass('active');
                $('.ap__dark').addClass('clickable');
            }
        }
    });
    
    $("#clear").click(function(){
        chrome.storage.local.set(
        {
            saved_items: []
        }, function(loaded){});
        location.reload();
    });
    $("#export").click(exportCSV);
    
    $('body').on("click", ".ap__select-color .clickable", function(e){
        e.preventDefault();
        $(".ap__box").toggleClass("ap__box--dark ap__box--white");
        $(".ap__select-color__links").toggleClass('clickable');
        
        var ui_theme = $(".ap__box").hasClass('ap__box--white')?'white':'dark';
        setTheme(ui_theme);
    });
    
    $('body').on('click', '.ap__remove', function(e){
        e.preventDefault();
        var id = $(this).parents('.ap__saved-table__line').attr('id');
        deleteRow(id);
        $(this).parents('.ap__saved-table__line').remove();
    });
    
    $('body').on('dblclick', '.notes', editNotes);
});

function deleteRow(uuid)
{
    chrome.storage.local.get(
    {
        saved_items: []
    }, function(loaded){

        var saved_items = loaded.saved_items;
        
        for(var i=0; i < saved_items.length; i++)
        {
            if(saved_items[i].id === uuid)
            {
                saved_items.splice(i,1);
            }
        };

        chrome.storage.local.set(
        {
            saved_items: saved_items
        }, function(loaded){});
    });
}

function editNotes(e)
{
    e.preventDefault();
    
    var uuid = $(this).parents('.ap__saved-table__line').attr('id');
    var value = $(this).text();
    
    bootbox.prompt({
        title: "Edit Notes",
        value: value,
        callback: function(result) {
            
            if(!result)
                return;
            
            chrome.storage.local.get(
            {
                saved_items: []
            }, function(loaded){

                var saved_items = loaded.saved_items;

                for(var i=0; i < saved_items.length; i++)
                {
                    if(saved_items[i].id === uuid)
                    {
                        saved_items[i].notes = result;
                    }
                }

                chrome.storage.local.set(
                {
                    saved_items: saved_items
                }, function(loaded){
                    $('#'+uuid).find('.notes').text(result);
                });
            });
        }
    });
}

function checkUncheckAllrows()
{
    if($(this).is(":checked"))
    {
        $("#html_output tbody").find("input[type=checkbox]").prop('checked', true);
    }
    else
    {
        $("#html_output tbody").find("input[type=checkbox]").prop('checked', false);
    }
}


/*
 * ---------------------------------------------------------------
 *  Hide Selected Rows
 * ---------------------------------------------------------------
 */
function hide_rows()
{
    var table = $("#html_output");
    table.find("tr").each(function(index){
        if($(this).find("input").is(":checked"))
            $(this).hide();
    });
}


/*
 * ---------------------------------------------------------------
 *  Show All Rows
 * ---------------------------------------------------------------
 */
function show_rows()
{
    var table = $("#html_output");
    table.find("tr").each(function(index){
            $(this).show();
    });
}

/*
 * ---------------------------------------------------------------
 *  Export table as csv
 * ---------------------------------------------------------------
 */
var csv_header = '"Name","ASIN","Rank","Buy Price","Sell Price","ROI","Profit","Date","Notes"';
function exportCSV()
{
    var output = csv_header + "\r\n";
    var trs = $(".ap__saved-table").find(".ap__saved-table__line");
    for(var x=0; x<trs.length; x++)
    {
        //var td = (x===0)?'th':'td';
        
        var tds = $(trs[x]).find('.ap__col');
        for(var k=0; k<tds.length; k++)
        {
            if(k ==0 || k == 10 || k == 11 )
                continue;
            
            output += '"' + $(tds[k]).text().trim().replace(/"/g, '') + '",';
        }
        output += "\r\n";
    }
    
    var a = document.createElement('a');
    a.href='data:text/csv;base64,' + btoa(output);
    a.download = filename + ".csv";
    a.click();
}

// Just a Helper Function
function replaceAll(text, search, replacement) {
    return text.replace(new RegExp(search, 'g'), replacement);
}


function addShownRowsForExport()
{
    var trs = $("#html_output tbody").find("tr:visible").clone();
    $("#saved_rows").append(trs);
}

function addSelectedRowsForExport()
{
    var trs = $("#html_output tbody").find("tr").each(function(){
        if($(this).find("input").is(":checked"))
        {
            $("#saved_rows").append($(this).clone());
        }
    });
    uncheckAllRows();
}

function uncheckAllRows()
{
    $("#html_output tbody").find("input[type=checkbox]").prop('checked', false);
}

function clearSavedRows()
{
    $("#saved_rows tbody").html("");
    uncheckAllRows();
}

function exportSavedRows()
{
    var output = csv_header + "\r\n";
    var trs = $("#saved_rows tbody").find("tr");
    for(var x=0; x<trs.length; x++)
    {
        var td = (x===0)?'th':'td';
        
        var tds = $(trs[x]).find('td');
        for(var k=0; k<tds.length; k++)
        {
            if(k == 0) continue;
            output += '"' + $(tds[k]).text().trim() + '",';
        }
        output += $(tds[1]).find('a').attr('href');
        output += ",*";
        output += "\r\n";
    }
    
    var a = document.createElement('a');
    a.href='data:text/csv;base64,' + btoa(output);
    a.download = filename + ".csv";
    a.click();
}


/* Get current date formatted */
function getCurrentDate()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = mm + dd + yyyy;
    return today;
}