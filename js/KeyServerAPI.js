var KeyServerAPI = function(){
    
    //var get_url = "http://localhost/keyserver/borrow/";
    //var return_url = "http://localhost/keyserver/return/";
    
    //var get_url = "http://70.40.220.107/~raythont/KeyServer/borrow/";
    //var return_url = "http://70.40.220.107/~raythont/KeyServer/return/";
    
    var get_url = "https://zally.io/KeyServer/borrow/";
    var return_url = "https://zally.io/KeyServer/return/";
    
    
    
    var token = "74f3f56d32fd24063b84c721d75940d8";
    var current_keyset = null;
    var key_issued_at = 0;
    
    this.getKeys = function(cb, country = 'us'){
        
        if(country === 'com')
            country = 'us';
        else if(country === 'co.uk')
            country = 'uk';
        else if(country === 'co.jp')
            country = 'jp';
        
        
        var current_public_key = '';
        if(current_keyset !== null)
            current_public_key = current_keyset.PublicKey;

        $.ajax({
                url: get_url + '?market=' + country,
                type: 'GET',
                data: {current: current_public_key},
                headers: {
                    "X-Token": token
                },
                success: function(r){
                    console.log(r);
                    if(!r || !r.status || r.status !== 'success' || !r.hasOwnProperty('data') || 
                            !r.data.hasOwnProperty('public') || !r.data.hasOwnProperty('private') || !r.data.hasOwnProperty('associate'))
                    {
                        cb(false);
                    }
                    else
                    {
                        current_keyset = {
                            PublicKey : r.data.public,
                            PrivateKey: r.data.private,
                            AssociateTag: r.data.associate
                        };

                        key_issued_at = Date.now();

                        cb(current_keyset);
                    }
            }
        }).fail(function(){
            cb(false);
        });
    }
    
    this.returnKey = function(isInvalid){
        
        if(current_keyset == null)
        {
            console.log("Return Key: No Key Found! nothing to return");
            return;
        }
        
        var data = {public: current_keyset.PublicKey};

        if(isInvalid)
            data.report_invalid = 1;

        $.ajax({
                    url: return_url,
                    type: 'POST',
                    data: data,
                    headers: {
                        "X-Token": token
                    },
                    success: function(r){
                        current_keyset = null;
            }
        });
    }
}

var KeyServer = new KeyServerAPI();

