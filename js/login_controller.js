function isLoggedIn(cb)
{
    chrome.storage.sync.get({ss_email: null}, function(loaded){
        if(loaded.ss_email != null)
            cb(true, loaded.ss_email);
        else
            cb(false);
    });
}

function checkLogin(email, pass, cb)
{
    var url = "https://zally.io/zally/api.php?email="+email+"&pass=" + pass;
    $.getJSON(url, function(data){
        if(data && data.hasOwnProperty("status") && data.status)
        {
            var email = data.email;
            if(!email)
                cb(false);
            else
            {
                chrome.storage.sync.set({ss_email: email}, function(){
                    cb(true);
                });
            }
        }
        else
        {
            cb(false);
        }
    }).fail(function(){
        cb(false);
    });
}

function logout()
{
    chrome.storage.sync.set({ss_email: null}, function(){
    });
}