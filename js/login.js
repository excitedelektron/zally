/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("#login").click(function(e){
        e.preventDefault();
        var email = $("#email").val();
        var pass = $("#password").val();
        
        if(email && pass)
        {
            checkLogin(email, pass, function(isLoggedIn){
                if(isLoggedIn)
                {
                    chrome.runtime.sendMessage({action: "OpenLastRequest"});
                    window.close();
                }
                else
                {
                    error("Login failed! Please make sure that you have entered correct email and password.");
                }
            });
        }
        else
            error("Please enter email and password");
    });
    
    function error(msg)
    {
        $('.log-status').addClass('wrong-entry');
        $('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
        
        /*
        $("#error").text(msg);
        $("#error").slideDown();
        setTimeout(function(){
            $("#error").slideUp();
        }, 5000);
        */
    }
});


/* Shake Effect */
$(document).ready(function(){
      $('.form-control').keypress(function(){
          $('.log-status').removeClass('wrong-entry');
      });
      $('#register').click(function(){
          window.location.href = "https://zally.io/zally/signup.php";
      });
  });