// v1.6.3
// Now result also returns product info on top of MFN and AFN fee
var AMZfeeCalc = function()
{
    this.calc = function (asin, sellPrice, buyPrice, mfn_sellPrice, country, callback)
    {
        var that = this;
        
        if(typeof country !== 'undefined')
            this.country = country;
        else
            this.country = 'com';
        
        this.language = countryLanguage[this.country];
        
        getNETPayout(asin, sellPrice, buyPrice, mfn_sellPrice, callback);
    }
    
    this.country = 'com';
    
    this.language = 'en_US';
    var netToken = "";
    var marketPlaceId = "";
    var currencyCode = "";
    var labelsAmazonFees = [];
    
    var countryLanguage = [];
    countryLanguage['de'] = 'de_DE';
    countryLanguage['it'] = 'it_IT';
    countryLanguage['es'] = 'es_ES';
    countryLanguage['co.uk'] = 'en_GB';
    countryLanguage['fr'] = 'fr_FR';
    countryLanguage['ca'] = 'en_CA';
    countryLanguage['com'] = 'en_US';
    countryLanguage['com.mx'] = 'es_MX';

    var countryCurrency = [];
    countryCurrency['de'] = 'EUR';
    countryCurrency['it'] = 'EUR';
    countryCurrency['es'] = 'EUR';
    countryCurrency['co.uk'] = '£';
    countryCurrency['fr'] = 'EUR';
    countryCurrency['ca'] = 'CDN$';
    countryCurrency['com'] = '$';
    countryCurrency['com.mx'] = '$';

    var tries = 0;
    var that = this;



    Number.prototype.format = function(n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };
    String.prototype.splice = function(idx, rem, str) {
        return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
    };


    function isNumber(value) {
        if ((undefined === value) || (null === value)) {
            return false;
        }
        if (typeof value == 'number') {
            return true;
        }
        return !isNaN(value - 0);
    }


    var getMoneySymbol = function() {
        return countryCurrency[country];
    }
    var getPureNumber = function(value) {
        if (value && !isNumber(value)) {
            return parseFloat(value.replace(/[^\d.-]/g, ''));
        }
        return value;
    }
    
    /*
     * ==================================
     *  Another One
     * ==================================
     */
    var getNETPayoutToken = function(callback)
    {
        $.get('https://sellercentral.amazon.'+that.country+'/fba/profitabilitycalculator/index?lang='+that.language, function(data){
            data = $(data);
            marketPlaceId = data.find("#marketplaceIdHidden").val();
            if (!marketPlaceId)
                marketPlaceId = 'A1F83G8C2ARO7P';

            currencyCode = data.find("#currencyCodeHidden").val();

            if (!currencyCode)
                currencyCode = 'GBP';

            data = $(data);
            netToken = data.find('input[name=profitcalcToken]').val();

            callback('success');
        }).fail(function(){
            tries++;

            if(tries < 10)
            {
                setTimeout(function(){
                    getNETPayoutToken(callback);
                }, tries * 2000);
            }
            else
            {
                callback('failed');
            }
        });
    };

    /*
     * ==================================
     *  Another One
     * ==================================
     */
    var getNETPayout = function (asin, sellPrice, buyPrice, mfn_sellPrice, callback)
    {   
        if (netToken == "")
        {
            tries = 0;
            getNETPayoutToken(function(res){
                if(res == 'success')
                {
                    getNETPayout(asin, sellPrice, buyPrice, mfn_sellPrice, callback);
                }
                else
                {
                    callback(false);
                    return;
                }
            });
            return;
        }

        var urlProductInfo = 'https://sellercentral.amazon.'+that.country+'/fba/profitabilitycalculator/productmatches?searchKey='+asin+'&language='+that.language+'&profitcalcToken='+netToken;

        $.getJSON(urlProductInfo, function( productInfo ) {

            productInfo = productInfo['data'][0];

            if (!productInfo) {
                console.log('Unable to fetch fee from Amazon server')
                return;
            }

            productInfo["selected"] = true;
            productInfo["language"] = that.language;
            productInfo["price"] = sellPrice;
            productInfo["revenueTotal"] = 0;
            productInfo["inbound-delivery"] = buyPrice ;
            productInfo["prep-service"] = 0 ;
            productInfo["fulfillmentTotal"] = 0 ;
            productInfo["undefined"] = 0 ;

            var netUrl = 'https://sellercentral.amazon.'+that.country+'/fba/profitabilitycalculator/getafnfee?profitcalcToken='+netToken;

            $.ajax({
                type: 'POST',
                url: netUrl,
                data: '{"productInfoMapping":' + JSON.stringify(productInfo) + ',"afnPriceStr":' + sellPrice + ',"mfnPriceStr":' + mfn_sellPrice + ',"mfnShippingPriceStr":0,"currency":"' + currencyCode + '","marketPlaceId":"' + marketPlaceId + '","hasFutureFee":false,"futureFeeDate":"2015-05-05 00:00:00"}',
                success: function(amz_response) { 
                    
                    
                    
                    var data = amz_response['data']['afnFees'];
                    
                    var result = {};

                    var fees = 0.00;

                    for (var name in data) 
                    {
                        var used = false;
                        var thisFee = 0.00;
                        var labelFee = '';

                        if (name == 'referralFee') {
                            used = true;
                            labelFee = 'Referral Fee';
                        }
                        if (name == 'pickAndPackFee') {
                            used = true;
                            labelFee = 'Pick and Pack Fee';
                        }
                        if (name == 'storageFee') {
                            used = true;
                            labelFee = 'Storage Fee';
                        }

                        if (that.country == 'com') {
                            if (name == 'orderHandlingFee') {
                                used = true;
                                labelFee = 'Order Handling Fee';
                            }

                            if (name == 'variableClosingFee') {
                                used = true;
                                labelFee = 'Variable Closing Fee';
                            }

                            if (name == 'fixedClosingFee') {
                                used = true;
                                labelFee = 'Fixed Closing Fee';
                            }

                            if (name == 'weightHandlingFee') {
                                used = true;
                                labelFee = 'Weight Handling Fee';
                            }
                        }

                        thisFee = getPureNumber(data[name]);

                        if (used) {
                            result[labelFee] = Math.round(thisFee*100)/100;
                            fees = getPureNumber(fees) + thisFee;
                        }
                    }
                    
                    result['total'] = Math.round(fees*100)/100;
                    
                    // Clean names array
                    var cleanName = {
                        'referralFee': 'Referral Fee',
                        'pickAndPackFee': 'Pick and Pack Fee',
                        'storageFee': 'Storage Fee',
                        'orderHandlingFee': 'Order Handling Fee',
                        'variableClosingFee': 'Variable Closing Fee',
                        'fixedClosingFee': 'Fixed Closing Fee',
                        'weightHandlingFee': 'Weight Handling Fee'
                    };
                    
                    // MFN Fee calculation
                    var mfn = {};
                    var total_mfn = 0;
                    
                    data = amz_response['data']['mfnFees'];
                    
                    for (var name in data) 
                    {
                        if (data.hasOwnProperty(name)) 
                        {
                            var label = cleanName[name]?cleanName[name]:name;
                            mfn[label] = data[name];
                            total_mfn += data[name];
                        }
                    }
                    
                    mfn['total'] = total_mfn;
                    
                    var final_result = {};
                    final_result['afn'] = result;
                    final_result['mfn'] = mfn;
                    final_result['productInfo'] = productInfo;

                    callback(final_result);
                },
                contentType: "application/json",
                dataType: 'json'
            }).fail(function(){
                console.log('Connectivity error (lv2).');
                callback();
            });
        }).fail(function(){
            console.log('Connectivity error (lv1). Resetting netToken and trying again...');
            netToken = "";
            getNETPayout(asin, sellPrice, buyPrice, mfn_sellPrice, callback);
        });

    };
}