var saved_items = [];
var last_request = '';

chrome.browserAction.onClicked.addListener(function (tab) {
    sendDataToTab({action: 'getStarted'}, tab.id);
    
    if(app_status !== 'ok')
    {
        chrome.runtime.openOptionsPage();
    }
});

// Listen for the updates from server, every half an hour
checkAppStatus();
setInterval(checkAppStatus, 30*60*1000); // check status every half hour
var status_message = '';
var app_status = 'ok';
function checkAppStatus()
{
    var url = "http://zally.io/zally/get_status.php?app=zally";
    $.get(url, function(data){
        if(data)
        {
            if(data.hasOwnProperty('status') && data.status !== 'ok')
            {
                switch(data.status) {
                    case 'yellow':
                        updateIconBadge('orange', '1');
                        break;
                    case 'red':
                        updateIconBadge('red', '1');
                        break;
                }
                
                // save the message recieved with status
                status_message = data.message;
                app_status = data.status;
                return;
            }
        }
        
        // In case no status is recieved, remove badge
        updateIconBadge('green', '');
    }, 'json');
    
    function updateIconBadge(color, text)
    {
        chrome.browserAction.setBadgeBackgroundColor({color: color});
        chrome.browserAction.setBadgeText({text: text});
    }
}


/*
 ****************************************
 * Listen for a request to get output
 ****************************************
 */

chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse)
        {
            var tab = sender.tab.id;
            var found = false;
            if (request.action == "GetBasicInfo")
            {
                var result = parseAmazonPage(request.html, request.country);

                var data = {};
                data.asin = result.ASIN;
                data.weight = result[getAttributeString('weight', request.country)];
                data.dim = result[getAttributeString('dimensions', request.country)];
                data.no_of_sellers = result["sellers_count"];
                data.amz_sells = result["amz_sells"];
                data.amz_price = result["price"];
                data.rank = result[getAttributeString('best_sellers_rank', request.country)];
                data.review_count = result['review_count'];

                sendResponse({status: 'success', data: data});
            } else if (request.action == "GetSellersInfo")
            {

                getSellersInfo(request.asin, request.country, function (result) {

                    var data = {};
                    data.asin = result.ASIN;
                    data.no_of_sellers = result["sellers_count"];
                    data.amz_sells = result["amz_sells"];
                    data.fba_count = result["fba_count"];
                    data.fbm_count = result["fbm_count"];
                    data.amz_price = result["price"];
                    data.low_fba = result["low_fba_price"];
                    data.high_fba = result["high_fba_price"];
                    data.low_fbm = result["low_fbm_price"];
                    data.high_fbm = result["high_fbm_price"];
                    data.fba_fpage = result["fba_fpage_avg"];
                    data.fbm_fpage = result["fbm_fpage_avg"];

                    sendDataToTab({status: 'success', type: 'sellers_info', data: data}, tab);
                });


            } else if (request.action == "CalcFee")
            {
                var feecalc = new AMZfeeCalc();
                feecalc.calc(request.asin, request.sell_price, request.buy_price, request.mfn_sell_price, request.country, function (ress) {
                    sendDataToTab({status: 'success', type: 'fees', data: ress}, tab);
                });
            } else if (request.action == 'save_product')
            {
                if (request.data)
                {
                    chrome.storage.local.get(
                            {
                                saved_items: []
                            }, function (loaded) {

                        saved_items = loaded.saved_items;

                        saved_items.push(request.data);

                        chrome.storage.local.set(
                                {
                                    saved_items: saved_items
                                }, function (loaded) {});
                    });

                }

            }
            else if(request.action === 'open_login_tab')
            {
                chrome.tabs.create({url: "/login.html"});
            }
            else if(request.action == "SaveLastRequest")
            {
                last_request = request.url;
            }
            else if(request.action == "OpenLastRequest")
            {
                if(last_request != "")
                {
                  chrome.tabs.create({url: last_request}, function(tab){
                    });
                }
                else
                {
                    chrome.runtime.openOptionsPage();
                }
            }
            else if(request.action === 'getStatusMessage')
            {
                sendResponse({status: app_status, message: status_message});
            }

            return true;
        });

var strings = [];
strings['weight'] = [];
strings['dimensions'] = [];
strings['best_sellers_rank'] = [];
strings['weight']['com'] = "Item Weight";
strings['weight']['de'] = "Artikelgewicht"; 
strings['weight']['fr'] = "Poids de l'article";
strings['weight']['it'] = "Peso articolo";
strings['weight']['es'] = "Peso del producto";
strings['weight']['com.mx'] = "Peso del producto";
strings['weight']['co.jp'] = "商品重量";
strings['dimensions']['com'] = "Product Dimensions";
strings['dimensions']['de'] = "Produktabmessungen"; 	
strings['dimensions']['fr'] = "Dimensions du produit (L x l x h)";
strings['dimensions']['it'] = "Dimensioni prodotto";
strings['dimensions']['es'] = "Dimensiones del producto";
strings['dimensions']['com.mx'] = "Dimensiones del producto";
strings['dimensions']['co.jp'] = "梱包サイズ";
strings['best_sellers_rank']['com'] = 'Best Sellers Rank';
strings['best_sellers_rank']['de'] = 'Amazon Bestseller-Rang';
strings['best_sellers_rank']['fr'] = 'Classement des meilleures ventes d\'Amazon';
strings['best_sellers_rank']['it'] = 'Posizione nella classifica Bestseller di Amazon';
strings['best_sellers_rank']['es'] = 'Clasificación en los más vendidos de Amazon';
strings['best_sellers_rank']['com.mx'] = 'Clasificación en los más vendidos de Amazon';
strings['best_sellers_rank']['co.jp'] = 'Amazon 売れ筋ランキング';

function getAttributeString(attribute, country)
{
    if (strings[attribute].hasOwnProperty(country))
        return strings[attribute][country];
    return strings[attribute]['com'];
}
function csv(s)
{
    return s;
}

/*
 * ***************************************************
 *  Extract Amazon Data
 * ***************************************************
 */
var sellers_count = 0;
function parseAmazonPage(html_data, country)
{
    return amzPageParser(html_data, country);
}

function getSellersInfo(asin, country, callback)
{
    if (!country)
        country = 'com';

    if (typeof asin === 'undefined')
        return "Unable to Extract Data";

    /*
     ****************************************************
     *  Sellers' details
     ****************************************************
     */

    var seller_link = "http://www.amazon." + country + "/gp/offer-listing/" + asin + "/ref=dp_olp_new?&overridePriceSuppression=1";

    var OfferListingIds = [];
    var OfferListingIds_fba = [];
    var OfferListingIds_fbm = [];
    var OfferListingIds_fpage = [];

    var sellers = [];
    sellers_count = 0;
    var fba_count = 0;
    var fbm_count = 0;
    var low_fbm_price = 0;
    var high_fbm_price = 0;
    var low_fba_price = 0;
    var high_fba_price = 0;
    var fba_sum = 0;
    var fbm_sum = 0;
    var fba_avg = 0;
    var fbm_avg = 0;

    var offer_list;

    var page_count = 0;

    get_sellers(seller_link, 0);

    function get_sellers(url, page_index)
    {
        $.get(url, handle_seller_page_response);
        function handle_seller_page_response(result) {

            var next_url = $("ul.a-pagination li.a-last a", result).first().attr('href');

            if (next_url)
            {
                next_url = "http://www.amazon." + country + next_url;
                get_sellers(next_url, page_index + 1);
                page_count++;
            }

            offer_list = $("#olpOfferList", result);
            if (offer_list.length > 0)
            {
                var offers = $(offer_list).find(".olpOffer");
                $(offers).each(function (index) {

                    var col1 = $(this).find("div").first();
                    var prime = $(col1).find(".supersaver");
                    // Get price
                    var base_price = $(col1).find(".olpOfferPrice").text().trim();
                    var shipping = $(col1).find(".olpShippingPrice").first().text();
                    base_price = Number(base_price.replace(/[^0-9\.]+/g, ""));
                    shipping = Number(shipping.replace(/[^0-9\.]+/g, ""));
                    var price = base_price + shipping;
                    price = +price.toFixed(2)

                    var fba = false;

                    if (prime.length > 0)
                    {
                        // FBA
                        fba = true;
                        if (fba_count == 0)
                        {
                            // first fba
                            low_fba_price = price;
                            high_fba_price = price;
                        } else
                        {
                            if (price < low_fba_price)
                                low_fba_price = price;
                            if (price > high_fba_price)
                                high_fba_price = price;
                        }
                        fba_count++;
                        fba_sum += price;
                    } else
                    {
                        // FBM
                        if (fbm_count == 0)
                        {
                            // first fbm
                            low_fbm_price = price;
                            high_fbm_price = price;
                        } else
                        {
                            if (price < low_fbm_price)
                                low_fbm_price = price;
                            if (price > high_fbm_price)
                                high_fbm_price = price;
                        }
                        fbm_count++;
                        fbm_sum += price;
                    }
                    sellers_count++

                    var offer_id = $(this).find("input[name='offeringID.1']").attr("value");
                    OfferListingIds.push(offer_id);

                    if (fba)
                        OfferListingIds_fba.push(offer_id);
                    else
                        OfferListingIds_fbm.push(offer_id);

                    // First Page
                    if (page_index == 0)
                    {
                        OfferListingIds_fpage.push(offer_id);

                        fba_avg = fba_sum / fba_count;
                        fbm_avg = fbm_sum / fbm_count;
                        fba_avg = +fba_avg.toFixed(2)
                        fbm_avg = +fbm_avg.toFixed(2)
                    }

                });
            }


            if (page_count == page_index)
            {
                var details = {};

                //asin = details["ASIN"];

                details["sellers_count"] = (sellers_count == 0) ? "N/A" : sellers_count;
                details["fba_count"] = (fba_count == 0) ? "N/A" : fba_count;
                details["fbm_count"] = (fbm_count == 0) ? "N/A" : fbm_count;
                details["low_fba_price"] = (low_fba_price == 0) ? "N/A" : low_fba_price;
                details["high_fba_price"] = (high_fba_price == 0) ? "N/A" : high_fba_price;
                details["low_fbm_price"] = (low_fbm_price == 0) ? "N/A" : low_fbm_price;
                details["high_fbm_price"] = (high_fbm_price == 0) ? "N/A" : high_fbm_price;
                details["fba_fpage_avg"] = fba_avg ? fba_avg : "N/A";
                details["fbm_fpage_avg"] = fbm_avg ? fbm_avg : "N/A";

                callback(details);
            }
        }
    }
}

/*
 * *********************************
 *  Send message to particular tab
 * *********************************
 */
function sendDataToTab(data, tabid)
{
    chrome.tabs.sendMessage(tabid, data, function (response) {

    });
}