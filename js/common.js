
function setTheme(theme_class)
{
    chrome.storage.sync.set({
        ui_theme: theme_class
    });
}

function getTheme(cb)
{
    chrome.storage.sync.get({
        ui_theme: 'dark'
    }, function(loaded){
        cb(loaded.ui_theme);
    });
}

function extractCountryFromURL(url)
{
    if(!url)
        return null;
    
    var found_country = '';
    var regex = /.+amazon\.([a-z.]{2,6})\/.*/;
    var matches = url.match(regex);
    
    if(matches.length == 0)
    {
        return null;
    }
    
    var found_country = matches[1];
    
    return found_country;
}

function adjustEuropeanNumber(num)
{
    num = num + " ";
    num = num.replace(/\,/g, '_');
    num = num.replace(/\./g, ',');
    num = num.replace(/_/g, '.');
    
    return num;
}

function isEuropeanMarket()
{
    var eu = ['de', 'fr', 'es', 'it'];
    var country = extractCountryFromURL(window.location.href);
    
    if(eu.indexOf(country) > -1)
        return true;
    return false;
}

// Checks from setting if user has marked the pro account option
function haveProAccount(callback)
{
    chrome.storage.sync.get({
        pro_account: true,
      }, function(items) {
      callback(items.pro_account);
    });
}

function round(n)
{
    return Math.round(n * 100)/100;
}