/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var compEngine = function()
{
    var snippet = function(id){
        this.id = id;
        this.quantity = getQuantity();
        this.buy_price = getBuyPrice();
        this.sell_price = getSellPrice();
        this.profit = 0;
        this.net_profit = 0;
        this.fee = 0;
        this.fee_total = 0;
        this.sellers = 0;
        this.weight = 1;
        this.inbound_shipping_per_lb = 0;
        this.inbound_shipping = 0;
        this.sales_tax_percent = 0;
        this.sales_tax = 0;
        this.other_costs = 0;
        this.extra_costs_total = 0;
        
        this.roi_target_1 = 10;
        this.roi_target_2 = 20;
        this.roi_target_3 = 40;
        
        this.roi_target_10 = 0;
        this.roi_target_20 = 0;
        this.roi_target_40 = 0;
        
        this.currency = '$';
        
        this.reCalculate = reCalculate;
        this.calculateFirstTimeBuyPrice = calculateFirstTimeBuyPrice;
        this.setWeight = setWeight;
        
        var that = this;
        
        loadSavedOptions(reCalculate);
        //reCalculate();
        
        function getBuyPrice()
        {
            return parseFloat(($("#buy_price_"+id).val()|| '').replace(/[^\d\.\-]/g,'')) || 0;
        }
        
        function getSellPrice()
        {
            return parseFloat(($("#sell_price_"+id).val() || '').replace(/[^\d\.\-]/g,'')) || 0;
        }
        
        function setBuyPrice()
        {
            $("#buy_price_"+id).val(that.buy_price);
        }
        
        function getQuantity()
        {
            return parseFloat($("#pimports_ext10 #quantity_"+id).val()) || 1;
        }
        
        function calculateROI(profit, buy_price)
        {
            return Math.round((profit/(buy_price + 0.001))*100);
        }
        
        function calculateTargetBuyPrice(target_roi)
        {
            //return round( ((that.sell_price - that.fee - that.extra_costs_total)*100)/(target_roi+100) );
            return round( ((that.sell_price - that.fee - that.inbound_shipping - that.other_costs)*100)/(target_roi + 100 + that.sales_tax_percent) );
        }
        
        function calculateFirstTimeBuyPrice()
        {
            that.buy_price = calculateTargetBuyPrice(0);
            setBuyPrice();
        }
        
        function reCalculate()
        {
            // Input
            that.quantity = getQuantity();
            that.buy_price = getBuyPrice();
            that.sell_price = getSellPrice();
            
            // Other Costs
            that.inbound_shipping = that.weight * that.inbound_shipping_per_lb;
            that.extra_costs_total = that.inbound_shipping;
            that.extra_costs_total += that.other_costs;
            that.sales_tax = that.sales_tax_percent * that.buy_price / 100;
            that.extra_costs_total += that.sales_tax;
            
            // Calculation
            //that.profit = Math.round((that.sell_price - that.buy_price - that.fee - that.extra_costs_total)*100)/100;
            that.profit = round((that.sell_price - that.buy_price - that.fee - that.extra_costs_total));
            that.roi = calculateROI(that.profit, that.buy_price);
            that.net_profit =  round( that.quantity * that.profit *100)/100;
            that.fee_total = round( that.quantity * that.fee *100)/100; 
            
            that.roi_target_10 = calculateTargetBuyPrice(that.roi_target_1); //round( ((that.sell_price - that.fee - that.extra_costs_total)*100)/(that.roi_target_1+100) );
            that.roi_target_20 = calculateTargetBuyPrice(that.roi_target_2); //round( ((that.sell_price - that.fee - that.extra_costs_total)*100)/(that.roi_target_2+100) );
            that.roi_target_40 = calculateTargetBuyPrice(that.roi_target_3); //round( ((that.sell_price - that.fee - that.extra_costs_total)*100)/(that.roi_target_3+100) );

            // Display
            setBuyPrice();
            $("#profit_"+id).text(numberWithCommas(that.profit));
            $("#net_profit_"+id).text(numberWithCommas(that.net_profit));
            $("#roi_"+id).text(numberWithCommas(that.roi) + '%');
            $("#pimp_fee_total_"+id+" .val").text(that.fee);
            $("#pimp_fee_total_final_"+id+" .val").text(that.fee_total);

            // Net Profit Tooltip
            $("#net_profit_sale_"+id).text(that.currency + round( that.quantity * that.sell_price ));
            $("#net_profit_buy_"+id).text(that.currency + round( that.quantity * that.buy_price ));
            $("#net_profit_fee_"+id).text(that.currency + round( that.quantity * that.fee ));
            $("#net_profit_inbound_"+id).text(that.currency + round( that.quantity * that.inbound_shipping ));
            $("#net_profit_sales_tax_"+id).text(that.currency + round( that.quantity * that.sales_tax ));
            $("#net_profit_other_costs_"+id).text(that.currency + round( that.quantity * that.other_costs ));
            $("#net_profit_net_profit_"+id).text(that.currency + that.net_profit);
            
            // Roi Targets
            $('.roi_target_1').text(that.roi_target_1 + '%');
            $('.roi_target_2').text(that.roi_target_2 + '%');
            $('.roi_target_3').text(that.roi_target_3 + '%');
            $("#roi_target_10_"+id).text(that.currency + that.roi_target_10);
            $("#roi_target_20_"+id).text(that.currency + that.roi_target_20);
            $("#roi_target_40_"+id).text(that.currency + that.roi_target_40);
            
            setROITargetColor(that.roi_target_1, '.roi_target_1');
            setROITargetColor(that.roi_target_2, '.roi_target_2');
            setROITargetColor(that.roi_target_3, '.roi_target_3');
            
            setROIColor(that.roi);
        }
        
        function setROIColor(roi)
        {
            var roi_class = 'ap__roi-color-1';
            if(roi < 0)
                roi_class = 'ap__roi-color-1';
            else if(roi >= 0 && roi < 10)
                roi_class = 'ap__roi-color-2';
            else if(roi >= 10 && roi < 50)
                roi_class = 'ap__roi-color-3';
            else if(roi >= 50 && roi < 100)
                roi_class = 'ap__roi-color-4';
            else
                roi_class = 'ap__roi-color-5';

            $("#roi_"+id).removeClass('ap__roi-color-1 ap__roi-color-2 ap__roi-color-3 ap__roi-color-4 ap__roi-color-5');
            $("#roi_"+id).addClass(roi_class);
        }
        
        function setROITargetColor(roi, selector)
        {
            var roi_class = 'ap__roi-color-1';
            if(roi < 0)
                roi_class = 'ap__roi-color-1';
            else if(roi >= 0 && roi < 10)
                roi_class = 'ap__roi-color-2';
            else if(roi >= 10 && roi < 50)
                roi_class = 'ap__roi-color-3';
            else if(roi >= 50 && roi < 80)
                roi_class = 'ap__roi-color-4';
            else
                roi_class = 'ap__roi-color-5';

            $(selector).removeClass('ap__roi-color-1 ap__roi-color-2 ap__roi-color-3 ap__roi-color-4 ap__roi-color-5');
            $(selector).addClass(roi_class);
        }
        
        function loadSavedOptions(cb)
        {
              chrome.storage.sync.get({
                    inbound_shipping: 0,
                    sales_tax: 0,
                    other_costs_fba: 0,
                    other_costs_mfn: 0,
                    
                    roi_target_1: 10,
                    roi_target_2: 20,
                    roi_target_3: 40
              }, function(items) {
                  that.inbound_shipping_per_lb = (that.id == 'fba')?(parseFloat(items.inbound_shipping)):0;
                  that.sales_tax_percent = parseFloat(items.sales_tax);
                  that.other_costs = parseFloat(items['other_costs_' + that.id]);
                  
                  var rt1 = parseFloat(items.roi_target_1);
                  var rt2 = parseFloat(items.roi_target_2);
                  var rt3 = parseFloat(items.roi_target_3);
                  
                  that.roi_target_1 = isNaN(rt1)?0:rt1;
                  that.roi_target_2 = isNaN(rt2)?0:rt2;
                  that.roi_target_3 = isNaN(rt3)?0:rt3;
                  
                  cb();
              });
        }
        
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        function round(n)
        {
            return Math.round(n * 100)/100;
        }
        
        function setWeight(weight)
        {
            that.weight = weight;
            that.inbound_shipping = that.weight * that.inbound_shipping_per_lb;
        }
    };
    
    this.fba = new snippet('fba');
    this.mfn = new snippet('mfn');
    
    this.setCurrency = function(curr)
    {
        this.fba.currency = curr;
        this.mfn.currency = curr;
    }
    
    this.setWeight = function(weightInPounds)
    {
        this.fba.setWeight(weightInPounds);
        this.mfn.setWeight(weightInPounds);
        
        $("#pimports_ext10 #_weight").text(weightInPounds + " pound(s)");
    }
}