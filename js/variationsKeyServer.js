/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var KeyServerForVariations = new KeyServerAPI();

chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse)
        {
            var tab = sender.tab.id;

            if (request.action == "GetKeys")
            {
                KeyServerForVariations.getKeys(function(keys){
                    sendResponse(keys);
                }, request.country);
            }
            else if(request.action == "ReturnKeys")
            {
                KeyServerForVariations.returnKey();
            }
            
            return true;
        });