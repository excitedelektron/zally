/* 
 * 
 ==============================================================
 Coded by Cashif Ilyas
 excitedelektron@yahoo.com
 ==============================================================
 All Rights Reserved.
 */


(function (fn) {

    if(document.readyState === "complete") {
      fn();
    } else {
      document.addEventListener('readystatechange', function () {
        if(document.readyState === "complete") {
          fn();
        }
      });
    }

  }(function () {

    var url = window.location.href;
    
    console.log(url);
    
    var spl = url.split('#');
    if(spl.length >1)
    {
        var hash = spl[1];
        var params = hash.split('&');
        if(params.length > 1)
        {
            var p1 = params[0].split('=');
            if(p1.length > 1 && p1[0] == 'asin')
            {
                var asin = p1[1];
                var p2 = params[1].split('=');
                if(p2.length > 1 && p2[0] == 'price')
                {
                    var price = p2[1];

                    document.getElementById('search-string').value = asin;
                    document.getElementById('search-string').focus();

                    setTimeout(function () {
                      document.querySelector('#search-form input[type="submit"]').click();
                    }, 1000);

                    document.getElementById('afn-fees-price').value = price;

                    setTimeout(function(){document.getElementById('update-fees-link-announce').click()},2000);
                }
            }
        }
    }
    

  }));
  
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}