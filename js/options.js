$(document).ready(function(){
    
    ui_controller.getSettings(function(){});
    
    $('#save_ui').click(function(){
        ui_controller.setSettings();
        $('#myModal').modal('hide')
    });
    
    $('.ap__editing-item').click(function(){
        $(this).toggleClass('active');
    });
    
    // Get App status
    chrome.runtime.sendMessage({
        action: "getStatusMessage"
    }, function(res){
        if(res && res.status && res.status != 'ok')
        {
            $("#app_status").addClass(res.status);
            $("#app_status").text(res.message);
        }
    });
});
/*
 * ------------------------------------------------
 *  Save Options
 * ------------------------------------------------
 */
function save_options() {
    
    var auto_open = document.getElementById('auto_open').checked;
    var pro_account = document.getElementById('pro_account').checked;
    var seller_limit = $('#seller_limit').val();
    var inbound_shipping = $('#inbound_shipping').val();
    var other_costs_fba = $('#other_costs_fba').val();
    var other_costs_mfn = $('#other_costs_mfn').val();
    var sales_tax = $('#sales_tax').val();
    
    var roi_target_1 = $("#roi_target_1").val();
    var roi_target_2 = $("#roi_target_2").val();
    var roi_target_3 = $("#roi_target_3").val();

    // Rank Stats
    var appendRankToSearchPage = document.getElementById('appendRankToSearchPage').checked;
    var appendRankToSponsoredProducts = document.getElementById('appendRankToSponsoredProducts').checked;
    var appendRankToAlsoBoughtProducts = document.getElementById('appendRankToAlsoBoughtProducts').checked;
    var appendRankToFrequentlyBougthProducts = document.getElementById('appendRankToFrequentlyBougthProducts').checked;
    var appendRankToCustomerProfile = document.getElementById('appendRankToCustomerProfile').checked;
    
    chrome.storage.sync.set({
            auto_open: auto_open,
            pro_account: pro_account,
            seller_limit: seller_limit,
            inbound_shipping: inbound_shipping,
            sales_tax: sales_tax,
            other_costs_fba: other_costs_fba,
            other_costs_mfn: other_costs_mfn,
            roi_target_1: roi_target_1,
            roi_target_2: roi_target_2,
            roi_target_3: roi_target_3,
            appendRankToSearchPage: appendRankToSearchPage,
            appendRankToSponsoredProducts: appendRankToSponsoredProducts,
            appendRankToAlsoBoughtProducts: appendRankToAlsoBoughtProducts,
            appendRankToFrequentlyBougthProducts: appendRankToFrequentlyBougthProducts,
            appendRankToCustomerProfile: appendRankToCustomerProfile
    }, function() {
            $("#status").text("Successfully saved!").slideDown();
    setTimeout(function() {
            $("#status").fadeOut();
        }, 2000);
    });
}

/*
 * ------------------------------------------------
 *  Load saved options
 * ------------------------------------------------
 */

function restore_options() {

  chrome.storage.sync.get({
        auto_open: true,
        pro_account: true,
        seller_limit: 30,
        inbound_shipping: 0,
        sales_tax: 0,
        other_costs_fba: 0,
        other_costs_mfn: 0,
        roi_target_1: 10,
        roi_target_2: 20,
        roi_target_3: 40,
        appendRankToSearchPage: true,
        appendRankToSponsoredProducts: true,
        appendRankToAlsoBoughtProducts: true,
        appendRankToFrequentlyBougthProducts: true,
        appendRankToCustomerProfile: true
  }, function(items) {
        document.getElementById('auto_open').checked = items.auto_open;
        document.getElementById('pro_account').checked = items.pro_account;
        $('#seller_limit').val(items.seller_limit);
        $('#inbound_shipping').val(items.inbound_shipping);
        $('#other_costs_fba').val(items.other_costs_fba);
        $('#other_costs_mfn').val(items.other_costs_mfn);
        $('#sales_tax').val(items.sales_tax);
        
        $('#roi_target_1').val(items.roi_target_1);
        $('#roi_target_2').val(items.roi_target_2);
        $('#roi_target_3').val(items.roi_target_3);

        document.getElementById('appendRankToSearchPage').checked = items.appendRankToSearchPage;
        document.getElementById('appendRankToSponsoredProducts').checked = items.appendRankToSponsoredProducts;
        document.getElementById('appendRankToAlsoBoughtProducts').checked = items.appendRankToAlsoBoughtProducts;
        document.getElementById('appendRankToFrequentlyBougthProducts').checked = items.appendRankToFrequentlyBougthProducts;
        document.getElementById('appendRankToCustomerProfile').checked = items.appendRankToCustomerProfile;
        
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',save_options);

/*--------  Login stuff  ---------*/
var zally_signup_url = "https://zally.io/zally/signup.php";
var zally_edit_url = "https://zally.io/zally/edit_profile.php";

$(document).ready(function(){
    isLoggedIn(function(isit, email){
        if(isit)
        {
            showLoggedIn(email);
        }
        else
        {
            showLoggedOut();
        }
    });
    
    $("#logout").click(function(){
        logout();
        showLoggedOut();
    });
    
    $("#edit_profile").click(function(){
        isLoggedIn(function(is, email){
            var url = zally_edit_url + "?email=" + email + "&token=" + generateRandomString() + "XD345";
            chrome.tabs.create({url: url}, function(tab){});
        });
        
    });
    
    $('body').on('click', '#login', function(e){
        e.preventDefault();
        window.close();
        chrome.tabs.create({url: "login.html"}, function(tab){});
    });
});

function showLoggedIn(email)
{
    $("#login_status").html("You are logged in as <b>" + email + "</b>");
    $("#login_status").addClass('green_font');
    $("#login_status").removeClass('red_font');
    $("#profile_options").show();
}

function showLoggedOut()
{
    $("#login_status").html("You are not logged in. Please <a href='login.html' id='login'>login</a> or <a href='" + zally_signup_url + "'>create a new account</a>");
    $("#login_status").removeClass('green_font');
    $("#login_status").addClass('red_font');
    $("#profile_options").hide();
}

function generateRandomString()
{
    return Math.random().toString(36).substring(7);
}