/* 
 * 
 ==============================================================
 Coded by Cashif Ilyas
 excitedelektron@yahoo.com
 ==============================================================
 All Rights Reserved.
 */


function amzPageParser(data, country = 'com')
{
    var result = data;
    if (typeof result === 'undefined' || result == "")
        return "Unable to Extract Data";

    var new_text = replaceAll(result, "onclick", "href");
    var new_text = replaceAll(new_text, "onerror", "fuck");
    var new_text = replaceAll(new_text, "<script", "<!--");
    var new_text = replaceAll(new_text, "</script>", "-->");
    var new_text = replaceAll(new_text, "<img", "<dpd");

    result = new_text;

    // Check if it is Amazon page
    if ($("#productTitle", result).length == 0)
        return "Unknown Data Format. Extraction Failed";

    var details = [];
    details["title"] = "";
    details["image"] = "";
    details["rank"] = "";
    details["category"] = "";
    details["review_count"] = "";
    details["review_rating"] = "";
    details["no_of_sellers"] = "";
    details["sellers_link"] = "";
    details["Item Weight"] = "";
    details["Product Dimensions"] = "";
    details["Best Sellers Rank"] = "";
    details["title"] = $("#productTitle", result).text();

    // Get Buy Box seller name
    var seller_name = 'Amazon.com';
    var m_info = $("#merchant-info", result);
    var a = $(m_info).find("a").first();
    if(a.length>0)
    {
        seller_name = $(a).text();
    }
    details['seller_name'] = seller_name;


    /*
     * ===============================================================
     *  First Layout Type
     * ===============================================================
     */

    if ($("#productDetails_detailBullets_sections1", result).length > 0)
    {
        $("#productDetails_detailBullets_sections1", result).find("tr").each(function (i) {
            var children = $(this).children();
            if (children.length > 0)
            {
                var key = $.trim($(children[0]).text());
                var value = $.trim($(children[1]).text());
                details[key] = value;
            }
        });
        var reviews = $("#acrCustomerReviewText", result);
        if (reviews.length > 0)
        {
            details["review_count"] = $(reviews).first().text();
        }
        details["review_rating"] = $(".reviewCountTextLinkedHistogram", result).attr("title");
        var sellers = $("#olp_feature_div a", result);
        if (sellers.length > 0)
        {
            var a = sellers.first();
            details["no_of_sellers"] = a.text();
            details["sellers_link"] = "http://www.amazon.com" + $(a).attr("href");
        }
    }
    /*
     * ===============================================================
     *  Second Layout Type
     * ===============================================================
     */
    else if ($("#detail-bullets", result).length > 0)
    {
        var content = $("#detail-bullets", result).find(".content");
        if (content.length > 0)
        {
            $(content).find("li").each(function (i) {
                var key = $.trim($(this).find("b").first().text());
                var value = $.trim($(this).clone().children().remove().end().text());
                // Omit last character
                key = key.substr(0, key.length - 1);
                details[key] = value;
            });
        }

        // Best sellers rank
        if (!!details["Amazon Best Sellers Rank"])
        {
            details["Best Sellers Rank"] = details["Amazon Best Sellers Rank"];
        }

        // Weight adjustement
        if (!!details["Shipping Weight"])
        {
            details["Item Weight"] = details["Shipping Weight"];
        }

        var reviews = $("#acrCustomerReviewText", result);
        if (reviews.length > 0)
        {
            details["review_count"] = $(reviews).first().text();
        }
        details["review_rating"] = $(".reviewCountTextLinkedHistogram", result).attr("title");
        // get link to sellers
        var sellers = $("#mbc", result).find("a");
        if (sellers.length > 0)
        {
            var a = sellers.first();
            details["no_of_sellers"] = a.text();
            details["sellers_link"] = "http://www.amazon.com" + $(a).attr("href");
        }

    }
    /*
     * ===============================================================
     *  Third Layout Type
     * ===============================================================
     */
    else if ($("#detailBullets", result).length > 0)
    {
        var content = $("#detailBullets", result).find("#detailBullets_feature_div");
        if (content.length > 0)
        {
            $(content).find(".a-list-item").each(function (i) {
                var key = $.trim($(this).find("span").first().text());
                var value = $.trim($(this).find("span").last().text());
                // Omit last character
                key = key.substr(0, key.length - 1);
                details[key] = value;
            });
        }

        // Best sellers rank
        var best_rank = $("#SalesRank", result);
        if (best_rank.length > 0)
        {
            details["Best Sellers Rank"] = $.trim($(best_rank).clone().children().remove().end().text());
        }

        // Weight adjustement
        if (!!details["Shipping Weight"])
        {
            details["Item Weight"] = details["Shipping Weight"];
        }

        var reviews = $("#acrCustomerReviewText", result);
        if (reviews.length > 0)
        {
            details["review_count"] = $(reviews).first().text();
        }
        details["review_rating"] = $(".reviewCountTextLinkedHistogram", result).attr("title");
        // get link to sellers
        var sellers = $("#mbc", result).find("a");
        if (sellers.length > 0)
        {
            var a = sellers.first();
            details["no_of_sellers"] = a.text();
            details["sellers_link"] = "http://www.amazon.com" + $(a).attr("href");
        }

    }
    /*
     * ===============================================================
     *  Fourth Layout Type
     * ===============================================================
     */
    else
    {
        // For the other format
        $(".techD table tr", result).each(function (i) {
            var children = $(this).children();
            if (children.length > 0)
            {
                var key = $.trim($(children[0]).text());
                var value = $.trim($(children[1]).text());
                details[key] = value;
            }
        });
        details["review_count"] = $("#averageCustomerReviewCount", result).text();
        details["review_rating"] = $("#averageCustomerReviewRating", result).text();
        // get link to sellers
        var sellers = $("#mbc", result).find("a");
        if (sellers.length > 0)
        {
            var a = sellers.first();
            details["no_of_sellers"] = a.text();
            details["sellers_link"] = "http://www.amazon.com" + $(a).attr("href");
        }
    }

    /* Extract only number from review count and review rating */
    if(!!details["review_count"])
    {
        var temp = details["review_count"].split(" ");
        if (temp.length > 0)
            details["review_count"] = temp[0];
    }

    if(!!details["review_rating"])
    {
        temp = details["review_rating"].split(" ");
        if (temp.length > 0)
            details["review_rating"] = temp[0];
    }

    /* Check if it is sold by Amazon */
    var sold_by = $("#merchant-info", result).text();
    if (sold_by.indexOf("sold by Amazon.com") > -1)
        details["amz_sells"] = 'Yes';
    else
        details["amz_sells"] = 'No';

    /* Price */
    var price = $("#priceblock_ourprice", result).text();
    details["price"] = price ? price : "N/A";

    /* Get sales rank and category */
    var all_ranks = details["Best Sellers Rank"].split("\n");
    if (all_ranks.length > 0)
    {
        var f_rank_splitted = all_ranks[0].split("(");
        if (f_rank_splitted.length > 0)
        {
            var s = f_rank_splitted[0].split(" in ");
            details["Best Sellers Rank"] = f_rank_splitted[0]; // Only first line
            if (s.length > 1)
            {
                details["rank"] = s[0].trim();
                details["category"] = s[1].trim();
            }

        }
    }
    
    details['image'] = $("#landingImage", result).attr("src");
    
    /* German Specific mining */
    if(country == 'de')
    {
        var regex_review_count = /([\d\.,]+) Kundenrezensionen/;
        var matches = result.match(regex_review_count);
        if(matches.length > 1)
        {
            details["review_count"] = matches[1];
        }

        var regex_review_rating = /([\d\.,]+) von 5 Sternen/;
        matches = result.match(regex_review_rating);
        if(matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
    }
    
    if(country == 'fr')
    {
        var regex_review_count = /([\d\.,]+) commentaires client/;
        var matches = result.match(regex_review_count);
        if(matches && matches.length > 1)
        {
            details["review_count"] = matches[1];
        }

        var regex_review_rating = /([\d\.,]+) étoiles sur 5/;
        matches = result.match(regex_review_rating);
        if(matches && matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
    }
    
    if(country == 'it')
    {
        var regex_review_count = /([\d\.,]+)  recensioni clienti/;
        var matches = result.match(regex_review_count);
        if(matches && matches.length > 1)
        {
            details["review_count"] = matches[1];
        }

        var regex_review_rating = /([\d\.,]+) su 5 stelle/;
        matches = result.match(regex_review_rating);
        if(matches && matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
    }
    
    if(country == 'es' || country == 'com.mx')
    {
        var regex_review_count = /([\d\.,]+) opinión de cliente|([\d\.,]+) opiniones de clientes/;
        var matches = result.match(regex_review_count);
        if(matches && matches.length > 1)
        {
            details["review_count"] = matches[matches.length-1];
        }

        var regex_review_rating = /([\d\.,]+) de un máximo de 5 estrellas/;
        matches = result.match(regex_review_rating);
        if(matches && matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
    }
    
    if(country == 'ca')
    {
        var regex_review_count = /([\d\.,]+) customer reviews/;
        var matches = result.match(regex_review_count);
        if(matches && matches.length > 1)
        {
            details["review_count"] = matches[matches.length-1];
        }

        var regex_review_rating = /([\d\.,]+) out of 5 stars/;
        matches = result.match(regex_review_rating);
        if(matches && matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
        
        // dimensions and weight
        var li = $('#detail_bullets_id .content li', result).first()
                    .clone().children().remove().end().text().trim();
        
        if(li)
        {
            var int2 = li.split(";");
            details["Product Dimensions"] = int2[0];
            details["Item Weight"] = int2[1];
        }
        
    }
    
    if(country == 'co.jp')
    {
        var regex_review_count = /([\d\.,]+)件のカスタマーレビュー/;
        var matches = result.match(regex_review_count);
        if(matches && matches.length > 1)
        {
            details["review_count"] = matches[matches.length-1];
        }

        var regex_review_rating = /5つ星のうち([\d\.,]+)/;
        matches = result.match(regex_review_rating);
        if(matches && matches.length > 1)
        {
            details["review_rating"] = adjustEuropeanNumber(matches[1]);
        }
        
        // dimensions and weight
        var li = $('#detail_bullets_id .content li', result).first()
                    .clone().children().remove().end().text().trim();
        
        if(li)
        {
            var int2 = li.split(";");
            details["Product Dimensions"] = int2[0];
            details["Item Weight"] = int2[1];
        }
        
    }
        

    return details;
}

function replaceAll(text, search, replacement) {
    return text.replace(new RegExp(search, 'g'), replacement);
}