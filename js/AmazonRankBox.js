﻿/// <reference path="AmazonHelper.js" />

var AmazonRankBox = function () {

    var _root = this;
    var _amazonHelper = new AmazonHelper();

    // SEARCH PAGE
    _root.appendToSearchPage = function () {

        $("li.s-result-item").each(function () {

            // don't append if already exist
            if ($(this).find(".zally-rank-div").length > 0) return;

            var li = $(this);
            var asin = li.attr("data-asin");
            var rankDiv = _root.getRankDivHtml(asin, true);

            li.find(".s-item-container").append(rankDiv);

            _root.getRankDiv(asin, null, function (div) {

                // zally-rank-div
                li.find(".zally-rank-div").replaceWith(div);
            });
        });

    };

    // Sponsored products
    _root.appendToSponsoredProducts = function () {

        $("li[role='listitem']").each(function () {

            // don't append if already exist
            if ($(this).find(".zally-rank-div").length > 0) return;

            var _div = $(this).find("div").first();
            var asin = _div.attr("data-asin");

            if (asin) {

                var rankDiv = _root.getRankDivHtml(asin, true);

                $(_div).append(rankDiv);
                $(_div).find(".zally-rank-div").addClass("zally-rank-div-small");


                _root.getRankDiv(asin, null, function (div) {
                    $(_div).find(".zally-rank-div").replaceWith(div);
                    $(_div).find(".zally-rank-div").addClass("zally-rank-div-small");
                });
            }
        });

    };

    // Customers who bought this item also bought
    _root.appendToAlsoBoughtProducts = function () {

        $("li[role='listitem']").each(function () {

            // don't append if already exist
            if ($(this).find(".zally-rank-div").length > 0) return;

            var _div = $(this).find("div").first();

            if (_div.attr("data-p13n-asin-metadata")) {

                var asin = JSON.parse(_div.attr("data-p13n-asin-metadata")).asin;

                if (asin) {

                    var rankDiv = _root.getRankDivHtml(asin, true);

                    $(_div).append(rankDiv);
                    $(_div).find(".zally-rank-div").addClass("zally-rank-div-small");

                    _root.getRankDiv(asin, null, function (div) {
                        $(_div).find(".zally-rank-div").replaceWith(div);
                        $(_div).find(".zally-rank-div").addClass("zally-rank-div-small");
                    });
                }
            }
        });

    };

    // Frequently bought together
    _root.appendToFrequentlyBougthProducts = function () {

        // Frequently bought together
        $(".sims-fbt-rows ul li").each(function () {

            // don't append if already exist
            if ($(this).find(".zally-rank-div").length > 0) return;

            var _div = $(this);
            var asin = JSON.parse(_div.attr("data-p13n-asin-metadata")).asin;

            if (asin) {

                var rankDiv = _root.getRankDivHtml(asin, true);

                $(_div).append(rankDiv);
                $(_div).find(".zally-rank-div").addClass("zally-rank-div-inline");

                _root.getRankDiv(asin, null, function (div) {
                    // zally-rank-div
                    $(_div).find(".zally-rank-div").replaceWith(div);
                    $(_div).find(".zally-rank-div").addClass("zally-rank-div-inline");
                });
            }
        });
    };

    // Amazon customer profile review page
    _root.appendToCustomerProfile = function () {

        $(".glimpse-card").each(function () {

            // don't append if already exist
            if ($(this).find(".zally-rank-div").length > 0) return;

            var _div = $(this);
            var asin = _div.find("#glimpse-ephemeral-metadata").attr("data-asin");

            if (asin) {

                var rankDiv = _root.getRankDivHtml(asin, true);

                _div.find(".glimpse-card-main").append(rankDiv);


                _root.getRankDiv(asin, null, function (div) {
                    // zally-rank-div
                    _div.find(".zally-rank-div").replaceWith(div);
                });
            }
        });
    };

    // setElementEvents - Click, Hover etc.
    _root.setElementEvents = function () {

        $(".zally-rank-price-history").unbind();
        $(".zally-rank-price-history").off();
        $(".zally-rank-price-history").on("click", function () {

            var url = $(this).attr("data-url");

            if (url) {

                var left = (document.body.clientWidth - $(".zally-rank-price-history-box").width()) / 2;
                $(".zally-rank-price-history-box").css("left", left + "px");

                $(".zally-rank-price-history-box").show();
                $(".zally-rank-price-history-box iframe").remove();
                $(".zally-rank-price-history-box").append("<iframe src='" + url + "' />");
            }
        });
    };

    // Get Rank Div with params
    _root.getRankDivHtml = function getRankDivHtml(asin, getLoadingTemplate, country, rankNumber, rankCategory, soldByAmazon, fbaSellerCount, hasMoreSellers) {

        country = country || "com";

        // Loading Rank Box
        if (getLoadingTemplate) {

            return "<div class='zally-rank-div'>\
                        \
                        <div class='zall-rank-div-row'>\
                            Loading rank...\
                        </div>\
                        \
                        <div class='zall-rank-div-row'>\
                            <b>ASIN:</b> <span class='asin'>"+ asin + "</span>\
                        </div>\
                        \
                    </div>";

        }

            // Filled Rank Box
        else {

            var priceHistoryKeepaUrl = "https://keepa.com/iframe_addon.html#1-0-" + asin;

            return "<div class='zally-rank-div'>\
                        \
                        <div class='zally-rank-div-row'>\
                            <span style='display:" + (rankNumber ? "inline-block" : "none") + "'>\
                            <b class='txt-blue'>#" + rankNumber + "</b> <b>in " + rankCategory + "</b></span>\
                            <span style='display:" + (!rankNumber ? "inline-block" : "none") + "'>Rank not found</span>\
                        </div>\
                        \
                        <div class='zally-rank-div-row'>\
                            <b class='asin hide-small'>ASIN:</b> <span class='asin'>" + asin + "</span><a href='javascript:void(0);' title='Click to open Price & BSR History Chart' class='zally-rank-price-history txt-blue left-border' data-url='" + priceHistoryKeepaUrl + "'>Price & BSR</a>\
                        </div>\
                        \
                        <div class='zally-rank-div-row'>\
                            <span class='sold-by-amazon txt-blue' style='display:" + (soldByAmazon ? "inline-block" : "none") + "'><a href='https://www.amazon." + country + "/gp/offer-listing/" + asin + "/ref=dp_olp_new_mbc?ie=UTF8&condition=new' target='_blank'>Sold by Amazon</a> | </span>\
                            <span class='fba-sellers txt-blue' style='display:" + (fbaSellerCount > 0 ? "inline-block" : "none") + "'><a href='https://www.amazon." + country + "/gp/offer-listing/" + asin + "/ref=dp_olp_new_mbc?ie=UTF8&condition=new' target='_blank'>" + fbaSellerCount + (hasMoreSellers ? "+" : "") + " " + (fbaSellerCount > 1 ? "FBA Sellers" : "FBA Seller") + "</a></span>\
                        </div>\
                        \
                    </div>";
        }

    };

    // Get Rank Div by ASIN value
    _root.getRankDiv = function (asin, country, callBack) {

        country = country || "com";

        _amazonHelper.getProductByASIN(asin, country, function (product) {

            _amazonHelper.getOffers(asin, country, function (offers, nextPageUrl) {

                var soldByAmazon = offers.filter(function (offer) { return offer.soldByAmazon }).length > 0;

                var fba = offers.filter(function (offer) { return offer.prime && !offer.soldByAmazon });
                fba = (fba && fba.length > 0) ? fba.length : false;

                var rankArr = (product.hasOwnProperty("rank") && product.rank) ? product.rank.split(' in ') : [];
                var rankNumber = rankArr[1] ? rankArr[0].replace("#", "").trim() : false;
                var rankCategory = rankArr[1] ? rankArr[1].trim() : false;
                var hasMoreSellers = (nextPageUrl && nextPageUrl.length > 0)

                var div = _root.getRankDivHtml(asin, false, country, rankNumber, rankCategory, soldByAmazon, fba, hasMoreSellers);

                callBack(div);
            });
        });


    };

    // Append History Box Modal
    _root.appendHistoryBoxModal = function () {

        if ($(".zally-rank-price-history-box").length == 0) {
            $("body").append("<div class='zally-rank-price-history-box'><a href='javascript:void(0);' class='zally-rank-price-history-box-close'>x close</a></div>");
            $(".zally-rank-price-history-box .zally-rank-price-history-box-close").on("click", function () { $(".zally-rank-price-history-box").hide() });
        }
    };

    function appendAllRankDivs(options) {

        _root.appendHistoryBoxModal();

        if (options.appendRankToSearchPage) _root.appendToSearchPage();
        if (options.appendRankToSponsoredProducts) _root.appendToSponsoredProducts();
        if (options.appendRankToAlsoBoughtProducts) _root.appendToAlsoBoughtProducts();
        if (options.appendRankToFrequentlyBougthProducts) _root.appendToFrequentlyBougthProducts();
        if (options.appendRankToCustomerProfile) _root.appendToCustomerProfile();

        _root.setElementEvents();
    };

    function getOptions(callBack) {

        chrome.storage.sync.get({
            appendRankToSearchPage: true,
            appendRankToSponsoredProducts: true,
            appendRankToAlsoBoughtProducts: true,
            appendRankToFrequentlyBougthProducts: true,
            appendRankToCustomerProfile: true
        }, function (options) {
            callBack(options);
        });
    };

    // Append All Rank Divs
    _root.appendAllRankDivs = function () {

        getOptions(function (options) {

            appendAllRankDivs(options);
            setInterval(function () { appendAllRankDivs(options); }, 2500);
        });
    };

};