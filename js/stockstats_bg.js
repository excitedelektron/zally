
/*
 *******************************************************
 *  	Extension by Cashif ilyas
 *	excitedelektron@yahoo.com
 *******************************************************
 */

var PrivateKey = "1WEMbEwpqgLKUCrnwzO1vQ5Q0gTWcKUyN8qmj8M9";
var PublicKey = "AKIAJCPYFY5YEXKFSI3Q";
var AssociateTag = "stockstats01-20";

//var PrivateKey = "";
//var PublicKey = "";
//var AssociateTag = "";

var wrong_aws_alert_shown = false;

/*
 * Extracts ASIN from given amazon url
 * @param {type} url
 * @returns {extractASINfromURL.parts|Boolean}
 */
function extractASINfromURL(url)
{
    if(!url)
        return false;
    
    var asin = false;
    
    var parts = url.split("/");
    if(parts.length > 4)
    {
        if(parts[2] == 'www.amazon.com' || parts[2].indexOf(".amazon.") > -1)
        {
            if(parts[3] == 'gp')
            {
                // sellers page
                asin = parts[5];
            }
            else if(parts[3] == 'dp' || parts[4] == 'dp')
            {
                // product page
                if(parts[3] == 'dp')
                    asin = parts[4];
                else if(parts[4] == 'dp')
                    asin = parts[5];
            }
            else if(parts[4] == 'product-reviews')
            {
                // reviews page
                asin = parts[5];
            }
        }
    }
    return asin;
}


/*
 ****************************************
 * Listen for extension button click 
 ****************************************
 */
var urls = [];
var seller_limit = 30;

/*
 ****************************************
 * Listen for a request to get output
 ****************************************
 */

chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse)
        {            
            var tab = sender.tab;
            if (request.action == "GetSellers")
            {
                wrong_aws_alert_shown = false;
                
                var cn = 'us';
                if(request.country == 'com')
                    cn = 'us';
                else if(request.country == 'co.uk')
                    cn = 'uk';
                else
                    cn = request.country;

                
                KeyServer.getKeys(function(keys){
                    if(keys)
                    {
                        PrivateKey = keys.PrivateKey;
                        PublicKey = keys.PublicKey;
                        AssociateTag = keys.AssociateTag;
                    }
                    else
                    {
                        //alert("Unable to get keys from the Key Server. Please report this error to the technical support team");
                        console.error("Unable to get Keys from the server");
                    }

                    var asin = request.asin;

                    if(!asin)
                        return;

                    var result = ss_extractData(asin, tab.id, request.country);
                }, cn);
                
                chrome.storage.sync.get({
                    seller_limit: 30
                }, function(items) {
                    seller_limit = items.seller_limit ? items.seller_limit : 30;
                });
            }
        });
        

/*
 * ***************************************************
 *  Extract Amazon Data
 * ***************************************************
 */
var seller_id = 3001;
function ss_extractData(asin, tabid, country)
{
    
    if(!country)
        country = 'com';
    
    
    if (typeof asin === 'undefined')
        return "Unable to Extract Data";
    
    /*
     ****************************************************
     *  Product Info
     ****************************************************
     */
    
    var url = "http://www.amazon."+ country +"/gp/product/" + asin;
    $.get(url, handler, "text").always(function(){
//        delete in_progress[url];
//        checkIfDone();
    });
    
    function handler(data)
    {
        var pinfo = amzPageParser(data);
        console.log(pinfo);
        sendDataToTab({type:'product_info', data:{product_name: pinfo['title'], image: pinfo['image'], buy_box: pinfo['seller_name'], asin:asin, best_seller: pinfo['Best Sellers Rank']}}, tabid);
    }


    /*
    ****************************************************
    *  Sellers' details
    ****************************************************
    */

    var OfferListingIds = [];

    var sellers = [];
    var offer_list;
    var page_count = 0;

    var seller_link = "http://www.amazon."+ country +"/gp/offer-listing/" + asin + "/ref=dp_olp_new?&overridePriceSuppression=1";
    get_sellers(seller_link, 0);

    function get_sellers(url, page_index)
    {
        $.get(url, handle_seller_page_response);
         function handle_seller_page_response(result){


              offer_list = $("#olpOfferList", result);
              if (offer_list.length > 0)
              {

                  $(offer_list).find(".olpOffer").each(function (index) {

                      // limit the sellers
                      if(sellers.length >= seller_limit)
                          return;

                      // Get Seller Name
                      var seller_name = '';
                      var seller_name_div = $(this).find(".olpSellerName");
                      if(seller_name_div.find("img").length > 0)
                          seller_name = seller_name_div.find('img').attr('alt');
                      else
                          seller_name = seller_name_div.text();
                      seller_name = seller_name.trim();
                      
                      // seller URL
                      var seller_url = "http://www.amazon."+ country + seller_name_div.find('a').attr('href');

                      // Rating
                      var seller_rating = $(seller_name_div).next('p').text().trim();//.find('i').text();

                      var col1 = $(this).find("div").first();
                      var prime = $(col1).find(".supersaver").length || ($(this).text().indexOf('Add-on Item') > -1);
                      
                      // Get price
                      var base_price = $(col1).find(".olpOfferPrice").text().trim();
                      var shipping = $(col1).find(".olpShippingPrice").first().text();
                      base_price = Number(base_price.replace(/[^0-9\.]+/g, ""));
                      shipping = Number(shipping.replace(/[^0-9\.]+/g, ""));
                      var price = base_price + shipping;
                      price = +price.toFixed(2)

                      var fba = false;

                      if (prime)
                      {
                          // FBA
                          fba = true;

                      } else
                      {
                          // FBM
                      }

                     var offer_id = $(this).find("input[name='offeringID.1']").attr("value");

                     seller_id++;
                     var seller = {
                                  id: seller_id,
                                  name: seller_name,
                                  rating: seller_rating,
                                  price: price,
                                  fba: fba,
                                  offer_id: offer_id,
                                  url: seller_url
                              };
                      sellers.push(seller);

                      sendDataToTab({type:'seller_info', seller:seller}, tabid);


                      OfferListingIds.push(offer_id);


                  });
              }
              
            var next_url = $("ul.a-pagination li.a-last a", result).first().attr('href');

            if(next_url)
            {
                if(sellers.length < seller_limit)
                {
                    next_url = "http://www.amazon." + country + next_url;
                    get_sellers(next_url, page_index+1);
                    page_count++;
                }
            }
              
            if(page_count == page_index)
            {
                request_count = 0;
                for(var l = 0; l < sellers.length; l++)
                {
                    setTimeout(getAllStock, 1010*l, [sellers[l].offer_id], tabid, "stock_value", sellers[l].id, country);
                    request_count++;
                }
            }
         }
    }
       
}

// AN array to hold all in progress stock requests. We return keys at the end
var in_progress_requests = [];
var request_count = 0;

/*
 * *************************************************************
 *  AWS Signing stuff
 * *************************************************************
 */

function getAllStock(offer_ids, tabid, identification, seller_id, country) {
    
    in_progress_requests.push(offer_ids[0]);
    
    if(!country)
        country = 'com';

    var tot = 0;

    var action = getAmazonItemInfo(offer_ids, country);
   
    $.get(action.url, function(rr){
        
        checkIfDone(offer_ids[0]);
        
        var xmlDoc = $.parseXML(rr);
        var xml = $(xmlDoc);

        xml.find("CartItem").each(function(o){
           var q = $(this).find("Quantity").text(); 
           q = Number(q);
           tot += q;
        });


        if(typeof tabid !== 'undefined')
        {
            sendDataToTab(
                    {
                        status: "success",
                        type: identification,
                        data: tot,
                        seller_id: seller_id
                    },
                    tabid
                    );
        }
    }, "text").fail(function(jqxhr){
        checkIfDone(offer_ids[0]);
        console.log(jqxhr.status);
        if(jqxhr.status == 403 && !wrong_aws_alert_shown)
        {
            alert("Your AWS credentials are not accepted by Amazon. Please recheck them and make sure that your are registered with the Amazon Associates of the respective country");
            wrong_aws_alert_shown = true;
        }
        sendDataToTab(
                    {
                        status: "failure",
                        type: identification,
                        data: "Failed",
                        seller_id: seller_id
                    },
                    tabid
                    );
    });
    
    function checkIfDone(key)
    {
        request_count--;
        for(var i = 0; i < in_progress_requests.length; i++)
        {
            if(in_progress_requests[i] == key)
                in_progress_requests.splice(i, 1);
        }
        
        if(in_progress_requests.length == 0 && request_count == 0)
        {
            KeyServer.returnKey();
        }
    }
    
    return tot;
}

function sha256(stringToSign, secretKey) {
    var hex = CryptoJS.HmacSHA256(stringToSign, secretKey);
    return hex.toString(CryptoJS.enc.Base64);
}

function timestamp() {
    var date = new Date();
    var y = date.getUTCFullYear().toString();
    var m = (date.getUTCMonth() + 1).toString();
    var d = date.getUTCDate().toString();
    var h = date.getUTCHours().toString();
    var min = date.getUTCMinutes().toString();
    var s = date.getUTCSeconds().toString();
    if (m.length < 2) {
        m = "0" + m;
    }
    if (d.length < 2) {
        d = "0" + d;
    }
    if (h.length < 2) {
        h = "0" + h;
    }
    if (min.length < 2) {
        min = "0" + min;
    }
    if (s.length < 2) {
        s = "0" + s
    }

    var date = y + "-" + m + "-" + d;
    var time = h + ":" + min + ":" + s;
    return date + "T" + time + "Z";
}

function getAmazonItemInfo(OfferListingIds, country) {
    
    if(!country)
        country = 'com';

    var parameters = [];
    parameters.push("AWSAccessKeyId=" + PublicKey);
    parameters.push("AssociateTag=" + AssociateTag);
    
    for(var i=1; i<=OfferListingIds.length; i++ )
    {
        parameters.push("Item."+i+".OfferListingId=" + OfferListingIds[i-1]);
        parameters.push("Item."+i+".Quantity=999");
    }
    
    parameters.push("ItemId=");
    parameters.push("Operation=CartCreate");
    parameters.push("ResponseGroup=Cart");
    parameters.push("Service=AWSECommerceService");
    parameters.push("Timestamp=" + encodeURIComponent(timestamp()));
    parameters.push("Version=2011-08-01");
    parameters.sort();
    var paramString = parameters.join('&');
    var signingKey = "GET\n" + "webservices.amazon."+country+"\n" + "/onca/xml\n" + paramString

    var signature = sha256(signingKey, PrivateKey);
    signature = encodeURIComponent(signature);
    var amazonUrl = "http://webservices.amazon."+country+"/onca/xml?" + paramString + "&Signature=" + signature;
    return {url: amazonUrl, params: paramString + "&Signature=" + signature};
}

/*
 * Temp: for earch
 */
function searchQuery(keywords, country) {
    
    if(!country)
        country = 'com';

    var parameters = [];
    parameters.push("AWSAccessKeyId=" + PublicKey);
    parameters.push("AssociateTag=" + AssociateTag);
    
    parameters.push("Operation=ItemSearch");
    parameters.push("Keywords=notebook");
    parameters.push("SearchIndex=All");
    parameters.push("ResponseGroup=ItemAttributes");
    //parameters.push("Service=AWSECommerceService");
    parameters.push("Timestamp=" + encodeURIComponent(timestamp()));
    //parameters.push("Version=2011-08-01");
    parameters.sort();
    var paramString = parameters.join('&');
    var signingKey = "GET\n" + "webservices.amazon."+country+"\n" + "/onca/xml\n" + paramString

    var signature = sha256(signingKey, PrivateKey);
    signature = encodeURIComponent(signature);
    var amazonUrl = "http://webservices.amazon."+country+"/onca/xml?" + paramString + "&Signature=" + signature;
    return amazonUrl;
}

/*
 * *********************************
 *  Send message to particular tab
 * *********************************
 */
function sendDataToTab(data, tabid)
{
    chrome.tabs.sendMessage(tabid, data, function(response) {
        
      });
}

/*
 * Get Query Parameters
 */
function getQueryParams(u) {
    var result = [];
    
    var broken_url = u.split("?");
    var query = (broken_url.length>1)?broken_url[1]:broken_url[0];
    
    var sURLVariables = query.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        result[sParameterName[0]] = decodeURIComponent(sParameterName[1]);
    }
    return result;
}