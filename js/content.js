/// <reference path="AmazonRankBox.js" />
var _amazonRankBox = new AmazonRankBox();

/* Load template */
var country = extractCountryFromURL(window.location.href);
var currencies = [];
currencies['com'] = '$';
currencies['ca'] = '$';
currencies['de'] = '€';
currencies['fr'] = '€';
currencies['es'] = '€';
currencies['it'] = '€';
currencies['co.uk'] = '£';
currencies['com.mx'] = '$';
currencies['co.jp'] = '￥';

var template_currency = currencies[country];

var tmpt_data = {};
tmpt_data.logo = chrome.extension.getURL('icon48.png');
tmpt_data.show_saved_url = chrome.extension.getURL('saved-items.html');
tmpt_data.theme = 'ap__box--dark';
tmpt_data.currency = template_currency;

var template = chrome.extension.getURL('inject.ejs')
var html = new EJS({url: template}).render(tmpt_data);

var not_logged_in_template = chrome.extension.getURL('not_logged_in.ejs')
var not_logged_in_html = new EJS({url: not_logged_in_template}).render({});

// Compute Engine
var engine;
var loadedFirstTime = true;


var asin;
$(document).ready(function () {

    _amazonRankBox.appendAllRankDivs();

    // Read options to auto load
    chrome.storage.sync.get({
        auto_open: true
    }, function (items) {
        if (items.auto_open)
            loadBox();
    });

    // check version of the extension
    var manifestData = chrome.runtime.getManifest();
    var version = manifestData.version;
    chrome.storage.sync.get({
        last_version: null
    }, function (data) {
        if (data.last_version)
        {
            if (data.last_version != version)
            {
                console.log("Extension updated");
                setVersion(version);

                readUpdates(function (updates) {
                    updates = updates.replace('\n', '<br />');
                    var dialog = bootbox.dialog({
                        title: 'Zally Updates',
                        message: updates,
                        buttons: {
                            confirm: {
                                label: '<i class="fa fa-check"></i> Okay'
                            }
                        }
                    });
                });
            }
        } else
        {
            setVersion(version);
        }
    });

    function setVersion(version)
    {
        console.log("saving verion" + version);
        chrome.storage.sync.set({
            last_version: version
        });
    }

    function readUpdates(cb)
    {
        var file = chrome.extension.getURL("updates.txt");
        $.get(file, function (text) {
            cb(text);
        });
    }


});

/*
 * ------------------------------------------------------------
 *  Function to load all the data and stuff
 * ------------------------------------------------------------
 */


function loadBox()
{

    engine = new compEngine();
    engine.setCurrency(template_currency);

    $("body").on("change", "#pimports_ext10 #quantity_fba, #sell_price_fba, #buy_price_fba", engine.fba.reCalculate);
    $("body").on("change", "#pimports_ext10 #quantity_mfn, #sell_price_mfn, #buy_price_mfn", engine.mfn.reCalculate);
    $("body").on("change", "#sell_price_fba, #sell_price_mfn", inquireFee);
    $("body").on("click", "#save", save_product);
    $("body").on("click", "#buy_price", function () {
        this.select();
    });
    $("body").on("click", "#login_trigger", function () {
        chrome.runtime.sendMessage({
            action: "open_login_tab"
        });
    });

    //setCommaToTextFields("#buy_price, #sell_price");
    init_variations();

    var isTargetPage = false;

    if ($("#superleafHeroImageGradient, #booksTitle, #title_feature_div").length > 0)
    {
        isTargetPage = true;
    } 


    if (isTargetPage)
    {
        var asin = $("input#ASIN").val();

        isSellable(asin, function (status) {
            
            var sellProductLink = "https://catalog-retail.amazon." + country + "/abis/syh/DisplayCondition/ref=dp_sdp_sell?_encoding=UTF8&ld=AMZDP&coliid=&asin=" + asin + "&colid=&qid=&sr=";
            
            if (status == "unable_to_fetch")
            {
                $("#sellable").text("Login Required");
                $("#sellable").attr('href', "http://sellercentral.amazon." + country);
            } else if (status == "not_sellable")
            {
                $("#sellable").text("Restricted");
                $("#sellable").attr('href', sellProductLink);
            } else
            {
                $("#sellable").text("Not Restricted");
                $("#sellable").attr('href', sellProductLink);
            }

        });

        /*
         * ------------------------------------
         *  Calculator Stuff
         * ------------------------------------
         */
        var price = $("#priceblock_ourprice, #priceblock_saleprice").first().text().replace(/[^\d\.,]+/g, '');
        
        if(!price)
        {
            price = $(".offer-price, .kindle-price").first().text().replace(/[^\d\.,]+/g, '') || "";
        }
        
        if (isEuropeanMarket())
            price = adjustEuropeanNumber(price);

        price = parseFloat(price) || 0.00;
        
        // Special case: for games with banners
        if(price > 0 && $("#priceblock_ourprice, #priceblock_saleprice").first().hasClass('priceToPayPadding'))
        {
            price = price / 100;
        }

        if (!price && $('#olp_feature_div').length > 0)
        {
            var text = $('#olp_feature_div').text().trim();

            text = text.replace('& FREE shipping', '+ $0.00 shipping');

            const regex = /[.]* from \$([\d\.]+) \+ \$([\d\.]+) shipping/g;
            let m;

            while ((m = regex.exec(text)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                if (isEuropeanMarket())
                {
                    m[1] = adjustEuropeanNumber(m[1]);
                    m[2] = adjustEuropeanNumber(m[2]);
                }

                var pprice = parseFloat(m[1]) || 0;
                var pshipping = parseFloat(m[2]) || 0;
                price = pprice + pshipping;
                price = price.toFixed(2);
            }
        }

        // For media with banner images
        if (!price)
        {
            price = $('#digital-button-price').first().text().trim().replace(/[^\d\.\,]/g, '');

            if (isEuropeanMarket())
                price = adjustEuropeanNumber(price);

            price = parseFloat(price) / 100.0;
        }


        isLoggedIn(function(loggedin){
            var html_inject = "";
            if(loggedin)
            {
                html_inject = html;
            }
            else
            {
                html_inject = not_logged_in_html;
                chrome.runtime.sendMessage({
                    action: 'SaveLastRequest',
                    url: window.location.href
                });
            }

            if ($("#superleafHeroImageGradient").length > 0)
            {
                // Games with banner image
                $("#featurebullets_feature_div").after(html_inject);
            } 
            else if( $('#booksTitle').length > 0 )
            {
                // For books
                $('#booksTitle').after(html_inject);
            }
            else
            {
                // For others
                $("#title_feature_div").after(html_inject);
            }

            var box = $("#pimports_ext10");

            // Load Theme
            getTheme(function (theme) {
                if (theme)
                {
                    $(".ap__box").removeClass("ap__box--dark");
                    $(".ap__box").removeClass("ap__box--white");
                    $(".ap__box").addClass('ap__box--' + theme);
                }
                $(box).show();
            });
            
            var country = extractCountryFromURL(window.location.href);
            if (!country)
                country = 'com';

            $(".link_reviews").attr("href", "http://www.amazon." + country + "/abc/product-reviews/" + asin);
            $(".link_sellers").attr('href', "http://www.amazon." + country + "/gp/offer-listing/" + asin);

            $(".link_calc").attr('href', "https://sellercentral.amazon." + country + "/hz/fba/profitabilitycalculator/index?lang=en_US#asin=" + asin + "&price=" + price);
            $(".link_details").attr('href', chrome.extension.getURL('stockstats_output.html') + "#" + asin + "|" + extractCountryFromURL(window.location.href));
            $(".link_keepa").attr('href', "https://keepa.com/#!product/1-" + asin);
            $(".link_camel").attr('href', "http://camelcamelcamel.com/w/product/" + asin);

            $("#_asin").text(asin);
            $("#sell_price_fba").val(price);
            $("#sell_price_mfn").val(price);

            console.log("setting sell price to: " + price);

            $("#buy_price_fba").val(Math.round((price * 0.8 * 100) / 100));
            $("#buy_price_mfn").val(Math.round((price * 0.8 * 100) / 100));

            engine.fba.reCalculate();
            engine.mfn.reCalculate();


            inquireFee(asin);

            // Request Background thread to get detailed data
            enquireBG(asin);

            //engine = new compEngine();
            console.log(engine);

            showHideUI();
        });
    }
}

// Load UI preferences from extension's saved settings
function showHideUI() {

    ui_controller.getSettings(function (load) {

        for (var key in load) {
            if (load.hasOwnProperty(key)) {
                if (!load[key])
                    $('.' + key).hide();
            }
        }
    });
}

/*
 * ===========================================
 *  Inquire Fee from bg
 * ===========================================
 */
function inquireFee()
{
    var asin = $("input#ASIN").val();
    var country = extractCountryFromURL(window.location.href);
    $("#profit").text("Loading...");
    chrome.runtime.sendMessage({
        action: 'CalcFee',
        asin: asin,
        country: country,
        sell_price: engine.fba.sell_price,
        buy_price: engine.fba.buy_price,
        mfn_sell_price: engine.mfn.sell_price
    });
}

/*
 * -------------------------------------------------------------
 *  Function to enquire background page about product details
 * -------------------------------------------------------------
 */
function enquireBG(asin, forceRefresh)
{

    chrome.runtime.sendMessage({action: "GetBasicInfo", asin: asin, html: document.all[0].outerHTML, country: extractCountryFromURL(window.location.href)}, function (response)
    {

        if (typeof response !== 'undefined')
        {
            if (response.status == 'success')
            {
                populateBasicInfo(response.data);
            }
        }

    });

    chrome.runtime.sendMessage({action: "GetSellersInfo", asin: asin, html: document.all[0].outerHTML, country: extractCountryFromURL(window.location.href)}, function (response)
    {

        if (typeof response !== 'undefined')
        {
            if (response.status == 'success')
            {

            }
        }

    });
}

function populateSellersInfo(data)
{
    $("#_no_sellers").text(data.no_of_sellers);
    $("#_amz_sells").text(data.amz_sells);
    $("#_fba").text(data.fba_count);
    $("#_fbm").text(data.fbm_count);
    $("#_amz").text(data.amz_price);
    $("#_low_fba").text('$' + data.low_fba);
    $("#_high_fba").text('$' + data.high_fba);
    $("#_low_fbm").text('$' + data.low_fbm);
    $("#_high_fbm").text('$' + data.high_fbm);
    $("#_fba_fpage").text('$' + data.fba_fpage);
    $("#_fbm_fpage").text('$' + data.fbm_fpage);
}

// Function to display basic product information
function populateBasicInfo(data)
{
    $("#amz_seller_rank").text(data.rank);
    $("#_asin").text(data.asin);
    $("#_weight").text(data.weight ? data.weight.split('(')[0] : 'N/A');
//    $("#_no_sellers").text(data.no_of_sellers);
//    $("#_amz_sells").text(data.amz_sells);
//    $("#_fba").text(data.fba_count);
//    $("#_fbm").text(data.fbm_count);
//    $("#_amz").text(data.amz_price);
//    $("#_low_fba").text('$' + data.low_fba);
//    $("#_high_fba").text('$' + data.high_fba);
//    $("#_low_fbm").text('$' + data.low_fbm);
//    $("#_high_fbm").text('$' + data.high_fbm);
//    $("#_fba_fpage").text('$' + data.fba_fpage);
//    $("#_fbm_fpage").text('$' + data.fbm_fpage);
    $(".ap__reviews-number").text(compactNumber(data.review_count));

    // Review bar
    var review_class_name = 'ap__progress-bar--1'
    var review_count = +data.review_count;
    if (review_count == 0)
        review_class_name = 'ap__progress-bar--1';
    else if (review_count > 0 && review_count <= 10)
        review_class_name = 'ap__progress-bar--2';
    else if (review_count > 10 && review_count <= 50)
        review_class_name = 'ap__progress-bar--3';
    else if (review_count > 50 && review_count <= 100)
        review_class_name = 'ap__progress-bar--4';
    else
        review_class_name = 'ap__progress-bar--5';

    $('.ap__progress-bar__item').addClass(review_class_name);

    /* Split Dimensions into W H D */
    var dim = data.dim ? data.dim.split('x') : ['0', '0', '0'];
    if (isEuropeanMarket())
    {
        dim[0] = adjustEuropeanNumber(dim[0]);
        dim[1] = adjustEuropeanNumber(dim[1]);
        dim[2] = adjustEuropeanNumber(dim[2]);
    }
    dim[0] = dim[0].replace(/[^\d\.]/g, '') || 0;
    dim[1] = dim[1].replace(/[^\d\.]/g, '') || 0;
    dim[2] = dim[2].replace(/[^\d\.]/g, '') || 0;
    $("#_dim_w").text(dim[0] + '"');
    $("#_dim_h").text(dim[1] + '"');
    $("#_dim_d").text(dim[2] + '"');

    // Also get estimated sales
    if (data.rank)
    {
        var spl1 = data.rank.split(" in ");
        var rank = spl1[0].substr(1);
        var category = spl1[1];
        estimateSales(rank, category, function (sales) {
            $("#_sales").text(sales ? Number(sales).toLocaleString() : 'N/A');
        });

        var top_place = estimateTopPlace(rank, category);
        if (top_place)
        {
            $("#amz_seller_rank").text($("#amz_seller_rank").text() + " (Top: " + top_place + "%)");
        }
    }
}


/*
 * ------------------------------------
 *  Listen for response
 * ------------------------------------
 */

chrome.runtime.onMessage.addListener
        (
                function (request, sender, sendResponse)
                {
                    if (!!request)
                    {
                        if (request.hasOwnProperty("status") && request.status == "success")
                        {
                            switch (request.type)
                            {
                                case "sellers_info":
                                    populateSellersInfo(request.data);
                                    break;
                                case "fees":
                                    populateFee(request.data);
                                    break;
                            }
                        } else
                        {
                            switch (request.action)
                            {
                                case "getStarted":
                                    if ($('#pimports_ext10').length == 0)
                                        loadBox();
                                    break;
                            }
                        }
                    }
                }
        );


// Recieves an object with both AFN and MFN fees
function populateFee(response)
{
    haveProAccount(function (haveProAccount) {
        var tbl = $("#fee_table_fba").first();
        $(tbl).html('');

        var data = response['afn'];

        for (var key in data)
        {
            if (data.hasOwnProperty(key))
            {
                if (key == "total")
                    continue;
                
                if(key == "Fixed Closing Fee" && haveProAccount)
                {
                    response['afn']['total'] = round(parseFloat(response['afn']['total']) - parseFloat(data[key]));
                    data[key] = 0;
                }

                var html = "<tr><td>" + key + "</td><td><span class='currency'>"+ template_currency +"</span>" + data[key] + "</td></tr>";
                $(tbl).append(html);
            }
        }

        var tbl = $("#fee_table_mfn").first();
        $(tbl).html('');

        data = response['mfn'];

        for (var key in data)
        {
            if (data.hasOwnProperty(key))
            {
                if (key == "total")
                    continue;
                
                if(key == "Fixed Closing Fee" && haveProAccount)
                {
                    response['mfn']['total'] = round(parseFloat(response['mfn']['total']) - parseFloat(data[key]));
                    data[key] = 0;
                }

                var html = "<tr><td>" + key + "</td><td><span class='currency'>"+ template_currency +"</span>" + data[key] + "</td></tr>";
                $(tbl).append(html);
            }
        }

        engine.mfn.fee = response['mfn']['total'];
        engine.fba.fee = response['afn']['total'];
        
        // Get weight from productInfo and set into compute engine
        // This weight is used for calculation of inbound shipping cost
        if(response.productInfo && response.productInfo.hasOwnProperty('weight'))
            engine.setWeight(response['productInfo']['weight']);
        else
            console.log("Didn't recieve product weight from AMZ.");

        if(loadedFirstTime)
        {
            loadedFirstTime = false;
            engine.mfn.calculateFirstTimeBuyPrice();
            engine.fba.calculateFirstTimeBuyPrice();
        }
        
        engine.mfn.reCalculate();
        engine.fba.reCalculate();

    });
}

/*
 * =============================================
 *  Get estimate sales
 * ============================================
 */
function estimateSales(rank, category, callback)
{
    rank = rank.replace(/,/g, '');
    category = encodeURIComponent(category.trim());

    var url = "https://www.amztracker.com/unicorn.php?category=" + category + "&rank=" + rank;
//    var url = "https://junglescoutpro.herokuapp.com/api/v1/est_sales?store=us&rank="+rank+"&category="+category;
    $.get(url, function (data) {
        if (data)
        {
            callback(data);
        }
    }).fail(function () {
        callback(false);
    });
}

function estimateTopPlace(rank, category)
{
    rank = rank.replace(/,/g, '');
    rank = parseInt(rank);
    var target = category.toLowerCase().trim().replace(/and/g, '&');
    var cats = {'jewelry': 3367581, 'kindle store': 133140011, 'kitchen & housewares': 284507, 'magazine subscriptions': 599858, 'movies & tv': 2625373011, 'mp3 downloads': 2334092011, 'music': 5174, 'musical instruments': 11091801, 'office products': 1064954, 'pet supplies': 2619533011, 'shoes': 672123011, 'software': 229534, 'specialty stores': -4505, 'sports & outdoors': 3375251, 'tools & hardware': 228013, 'toys & games': 5407675, 'warehouse deals': 1267877011, 'apparel & accessories': 1036592, 'appstore for android': 2350149011, 'arts, crafts & sewing': 2617941011, 'automotive': 15684181, 'baby': 165796011, 'beauty': 3760911, 'black friday sales': 384082011, 'books': 283155, 'camera & photo': 502394, 'car toys': 10963061, 'cell phones & accessories': 2335753011, 'computer & video games': 468642, 'computers': 541966, 'electronics': 172282, 'grocery & gourmet food': 16310211, 'health & personal care': 3760901, 'home & kitchen': 1055398, 'industrial & scientific': 16310161};
    for (var i in cats)
    {
        var key = i;
        var val = cats[i];

        if (key.indexOf(target) > -1)
            return Math.round((rank / val) * 10000) / 100;
    }

    return null;
}

function save_product(e)
{
    var that = this;

    e.preventDefault();

    bootbox.prompt("Notes:", function (result) {
        if (result === null) {

        } else {
            var data = {};
            data.id = uuid();
            data.title = $("#productTitle").text().trim() || "N/A";
            data.image = $("#landingImage").attr("src");
            data.asin = $("#_asin").text();
            data.url = window.location.href;
            data.buy_price = engine.fba.buy_price;
            data.sell_price = engine.fba.sell_price;
            data.date = getDate();
            data.sales_rank = $("#amz_seller_rank").text();
            data.roi = engine.fba.roi;
            data.profit = engine.fba.profit;
            data.notes = result;
            data.stockstats_link = $('.link_details').attr('href');
            chrome.runtime.sendMessage({action: 'save_product', data: data});
            $(that).text("Saved");
        }
    });


}

function isSellable(asin, callback)
{
    var url = "https://sellercentral.amazon.com/sellyourproducts/" + asin + "/offers/create/simple";

    $.get(url, function (data) {
        if (data.indexOf("Sign in to your account") > -1)
        {
            callback("unable_to_fetch");
        } else
        {
            var temp = $(data).find("input[name=gatingDetailsHidden]");
            var check = $(data).find("input[name=gatingDetailsHidden]").attr('data-restrictedforallconditions');
            if (check == 'true')
            {
                callback("not_sellable");
            } else
            {
                callback("sellable");
            }
        }
    }).fail(function () {
        callback("unable_to_fetch");
    });
}

/*
 * Extracts ASIN from given amazon url
 * @param {type} url
 * @returns {extractASINfromURL.parts|Boolean}
 */
function extractASINfromURL(url)
{
    if (!url)
        return false;

    var asin = false;

    var parts = url.split("/");
    if (parts.length > 4)
    {
        if (parts[2] == 'www.amazon.com')
        {
            if (parts[3] == 'gp')
            {
                // sellers page
                asin = parts[5];
            } else if (parts[3] == 'dp' || parts[4] == 'dp')
            {
                // product page
                if (parts[3] == 'dp')
                    asin = parts[4];
                else if (parts[4] == 'dp')
                    asin = parts[5];
            } else if (parts[4] == 'product-reviews')
            {
                // reviews page
                asin = parts[5];
            }
        }
    }
    return asin;
}


function getDate()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    return today;
}

function setCommaToTextFields(selector)
{
    $("body").on("keyup", selector, function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40)
            return;

        // format number
        $(this).val(function (index, value) {
            return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
        });
    });
}

function compactNumber(num)
{
    if (num == null)
        return null;

    num = num.replace(/[^\d.]/g, '');
    num = parseFloat(num) || 0;
    var symbols = ['', 'K', 'M', 'G', 'T'];

    var i = 0;
    while (num >= 1000)
    {
        i++;
        num = num / 1000;
    }

    return  Math.floor(num) + symbols[i];
}

function uuid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
}

/* Formats number */
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}