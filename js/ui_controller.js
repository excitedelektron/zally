
UIController = function(){
    this.getSettings = function(cb){
        chrome.storage.sync.get({
            ui_roi_fba: true,
            ui_roi_mfn: true,
            ui_fba_sellers: true,
            ui_mfn_sellers: true
        }, function(load){
            
            for(var key in load)
            {
                if(load.hasOwnProperty(key))
                {
                    if(load[key])
                        $('#' + key).addClass('active');
                    else
                        $('#' + key).removeClass('active');
                }
            }
    
            cb(load);
        });
    };
    
    this.setSettings = function(){
        
        chrome.storage.sync.set({
            ui_roi_fba: $('#ui_roi_fba').hasClass('active'),
            ui_roi_mfn: $('#ui_roi_mfn').hasClass('active'),
            ui_fba_sellers: $('#ui_fba_sellers').hasClass('active'),
            ui_mfn_sellers: $('#ui_mfn_sellers').hasClass('active')
        }, function(load){
        });
    };
};

var ui_controller = new UIController();