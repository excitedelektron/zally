var PrivateKey = "1WEMbEwpqgLKUCrnwzO1vQ5Q0gTWcKUyN8qmj8M9";
var PublicKey = "AKIAJCPYFY5YEXKFSI3Q";
var AssociateTag = "stockstats01-20";

/*
 *  =========================================================
 *   First function after variation button is clicked
 *  =========================================================
 */
function init_variations(e)
{
    var asin = (e && e.length > 0) ? e : $("input#ASIN").val();

    var country = extractCountryFromURL(window.location.href);
    
    chrome.runtime.sendMessage({
        action: "GetKeys",
        country: country
    }, function(keys)
    {
        if(keys)
        {
            PrivateKey = keys.PrivateKey;
            PublicKey = keys.PublicKey;
            AssociateTag = keys.AssociateTag;
        }
        else
        {
            console.error("Variations: unable to get keys from server. Using the default one.");
        }

        getVariations(asin, country, function (vars) {

            chrome.runtime.sendMessage({
                action: "ReturnKeys"
            });

            $("#pimp_variations_div tbody").html("");

            for(var i=0; i<vars.length; i++)
            {
                var itm = vars[i];

                var html = '<tr>';
                html += '<td>'+itm.asin+'</td>';
                html += '<td>'+itm.size+'</td>';
                html += '<td>'+itm.color+'</td>';
                html += '<td>'+itm.price+'</td>';
                html += '<td>'+itm.offers+'</td>';
                html += '<td id="ext10_reviews_'+itm.asin+'"></td>';
                html += '</tr>'

                $("#pimp_variations_div tbody").append(html);
            }

            $("#pimp_variations_div table").tablesorter();
        });

    });
    
}


/*
 * *************************************************************
 *  AWS Signing stuff
 * *************************************************************
 */

function getVariations(asin, country, callback)
{
    // https://revseller.com/api/getVariations/B01GPKSCWQ:B01HTDLD4C
    // https://www.amazon.com/gp/product/B017W0F488

    var action = amzVariationsLookup(asin, country);

    $.post(action.url, action.params, function(rr){
        var xmlDoc = $.parseXML(rr);
        var xml = $(xmlDoc);

        // Variations only works with ParentASIN
        var parentASIN = xml.find("Items").find("ParentASIN").first().text();
        if (parentASIN && parentASIN.length > 1 && parentASIN != asin) {
            chrome.runtime.sendMessage({ action: "ReturnKeys" });
            setTimeout(function () { init_variations(parentASIN); }, 3500);
        }

        else {

            var results = [];
            xml.find("Variations Item").each(function () {
                var item = {};
                item.asin = $(this).find('ASIN').text();
                item.color = $(this).find('Color').text();
                item.size = $(this).find('Size').text();
                item.price = $(this).find('FormattedPrice').first().text();
                item.offers = $(this).find("Offer").length;
                results.push(item);

                reviewCounter(item.asin, country);

            }).promise().done(function () {
                callback(results);
            });
        };
        
        
    }, "text");
}

function reviewCounter(asin, country)
{
    if(!country)
        country = 'com';
    
    var url = "https://www.amazon." + country + "/ss/customer-reviews/ajax/reviews/get/ref=cm_cr_arp_d_viewopt_fmt";
    var data = {
        sortBy:'recent',
        reviewerType:'all_reviews',
        formatType:'current_format',
        filterByStar:'',
        pageNumber:1,
        filterByKeyword:'',
        shouldAppend:'undefined',
        deviceType:'desktop',
        reftag:'cm_cr_arp_d_viewopt_fmt',
        pageSize:10,
        asin:asin,
        scope:'reviewsAjax6'
    };
    
    $.post(url, data, function(res){

        var count = 0;

        try {
            var firstPos = res.indexOf("\">Showing ");
            var lastPos = res.indexOf(" reviews</span>");
            var total = lastPos - firstPos;

            count = parseInt(res.substr(firstPos, total).split(' ').pop().trim().replace(",", "").replace(".", ""));
        }
        catch (ex) {
            console.log(ex);
            count = "NaN";
        }

        $('#ext10_reviews_' + asin).text(count);
        $("#pimp_variations_div table").trigger("update");

    }, 'text');
}

function occurrences(string, subString, allowOverlapping) {

    string += "";
    subString += "";
    if (subString.length <= 0) return (string.length + 1);

    var n = 0,
        pos = 0,
        step = allowOverlapping ? 1 : subString.length;

    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            ++n;
            pos += step;
        } else break;
    }
    return n;
}

function amzVariationsLookup(asin, country) {
    
    if(!country)
        country = 'com';
    
    var parameters = [];
    parameters.push("AWSAccessKeyId=" + PublicKey);
    parameters.push("AssociateTag=" + AssociateTag);
    parameters.push("Operation=ItemLookup");
    parameters.push("ItemId=" + asin);
    parameters.push("ResponseGroup=" + encodeURIComponent("Variations"));
    parameters.push("Service=AWSECommerceService");
    parameters.push("Timestamp=" + encodeURIComponent(timestamp()));
    parameters.sort();
    var paramString = parameters.join('&');
    var signingKey = "POST\n" + "webservices.amazon." + country + "\n" + "/onca/xml\n" + paramString

    var signature = sha256(signingKey, PrivateKey);
    signature = encodeURIComponent(signature);
    var amazonUrl = "https://webservices.amazon."+ country +"/onca/xml?";
    return {url: amazonUrl, params: paramString + "&Signature=" + signature};
}

function sha256(stringToSign, secretKey) {
    var hex = CryptoJS.HmacSHA256(stringToSign, secretKey);
    return hex.toString(CryptoJS.enc.Base64);
}

function timestamp() {
    var date = new Date();
    var y = date.getUTCFullYear().toString();
    var m = (date.getUTCMonth() + 1).toString();
    var d = date.getUTCDate().toString();
    var h = date.getUTCHours().toString();
    var min = date.getUTCMinutes().toString();
    var s = date.getUTCSeconds().toString();
    if (m.length < 2) {
        m = "0" + m;
    }
    if (d.length < 2) {
        d = "0" + d;
    }
    if (h.length < 2) {
        h = "0" + h;
    }
    if (min.length < 2) {
        min = "0" + min;
    }
    if (s.length < 2) {
        s = "0" + s
    }

    var date = y + "-" + m + "-" + d;
    var time = h + ":" + min + ":" + s;
    return date + "T" + time + "Z";
}


